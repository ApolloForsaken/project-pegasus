using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class Sun : MonoBehaviour {

	private List<Planet> planetList;
	private StepList<Planet> stepList;
	public int numberOfPlanets;
	public Planet planetPrefab;
	public float minimumGap;
	public float maximumGap;
	private List<Material> materials;

	private float startingPosX;

	// Use this for initialization
	protected void Start () {
		reset ();
	}

	abstract public string getPseudoCode();
	abstract public void run ();

	public void generatePlanet(int number) {
		Planet planetGO= Instantiate(planetPrefab) as Planet;
		
		Planet planet = (Planet) planetGO.GetComponent<Planet>();

		float scale= (number * planet.maximumScale / 100) + planet.minimumScale;
		float gap= Random.Range(minimumGap + scale/2, maximumGap + scale/2);

		if (materials.Count == 0) {
			materials = GameManagerScript.materials;
		}

		Material material= materials[Random.Range(0, materials.Count - 1)];
		materials.Remove(material);

		planet.initialize (transform, number, material, true);

		planetGO.transform.position= (new Vector3(startingPosX + gap, transform.position.y, transform.position.z));
		planetList.Add (planetGO);
		
		startingPosX+= gap;
	}

	public void reset() {
		materials = new List<Material>(GameManagerScript.materials);

		if (planetList != null) {
			foreach (Planet child in planetList) {
					Destroy (child.gameObject);
			}
		}

		planetList = new List<Planet> ();
		stepList = new StepList<Planet> (planetList);
		startingPosX = transform.position.x + 1;
	}

	protected List<Planet> getPlanetList() {
		return planetList;
	}

	protected StepList<Planet> getStepList() {
		return stepList;
	}

	public IEnumerator Run(int seconds) {
		while (stepList.hasNext()) {
			stepList.next();
			yield return new WaitForSeconds (seconds);
		}
	}

	public void runStepList() {
		StartCoroutine("Run", 3);
	}

	public void stopStepList() {
		StopCoroutine("Run");
	}

	public void runNextStep() {
		stepList.next();
	}

	public void runPreviousStep() {
		stepList.previous ();
	}
	
}