﻿using UnityEngine;
using System.Collections.Generic;

public class BubbleCompareStep : Step<Planet> {

	private int planetL;
	private int planetR;
	private bool swap;

	public BubbleCompareStep(int planetL ,int planetR ,bool swap) {
		this.planetL = planetL;
		this.planetR = planetR;
		this.swap = swap;
	}

	public override void run(ref List<Planet> planetList) {
		//Debug.Log ("Swap: "+ planetList[planet].ToString() + " with "+ planetList[pivot].ToString() + " " + swap);
		
		if (swap) {
			animate (planetList);
			algoCode.replaceLine(2, "Swap: " 		+ planetList[planetL].number + " > " + planetList[planetR].number );
			swapPlanets (ref planetList, planetL, planetR);
		} else {
			algoCode.replaceLine(3, "Don't Swap: " + planetList[planetL].number + " <= " + planetList[planetR].number );
		}
		
		DebugScript.debugPlanetList (planetList);
	}
	
	public override void reverse(ref List<Planet> planetList) {
		if (swap) {
			animate (planetList);
			swapPlanets (ref planetList, planetL, planetR);
		}
	}
	
	public static void swapPlanets(ref List<Planet> planetList, int planetL, int planetR) {
		
		Planet planetRtmp = planetList [planetR];
		Planet planetLtmp = planetList [planetL];
 
		planetList [planetR] = planetLtmp;
		planetList [planetL] = planetRtmp;
	  
	}


	
 	private void animate(List<Planet> planetList) {
		
		Planet planetRScript= planetList[planetR].GetComponent<Planet>();	
		Planet planetLScript= planetList[planetL].GetComponent<Planet>();
 
		planetRScript.move (planetList [planetL].transform.position, Vector3.down);
		planetLScript.move(planetList [planetR].transform.position, Vector3.up);
	 		
	}
	
	 
}
