﻿using UnityEngine;
using System.Collections.Generic;

public class QSPivotStep : Step<Planet> {

	private Planet pivot;

	public QSPivotStep(Planet pivot) {
		this.pivot = pivot;
	}

	public override void run(ref List<Planet> lst) {
		pivot.highlight (true, Color.yellow);
		algoCode.replaceLine(2, "Set Pivot: " + pivot.number);
	}

	public override void reverse(ref List<Planet> planetList) {
		pivot.highlight (false);
	}
}
