﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BTreeLeafScript : MonoBehaviour {

	public List<int> keys;
	public List<BTreeLeafScript> leaves;
	public TreeScript bTree;
	public BTreeLeafScript parentLeaf;
	public int parentLeafPos;
	public int level;

	public LineRenderer line;

	// Utils
	public Color leafColor;
	public Color defaultLineStartColor;
	public Color defaultLineEndColor;
	public Color lineHighlightColor;
	public Color defaultNumberColor;
	public Color insertColor;
	public Color deleteColor;
	public float lineWidthStart;
	public float lineWidthEnd;

	public void Awake() {
		bTree = GameManagerScript.treeScript;
		transform.parent = bTree.transform;
		line= GetComponent<LineRenderer> ();
		line.enabled = false;
	}

	public void Start() {
		
	}

	public void Update() {
		
		line.SetPosition (0, transform.position);
		if(!isRoot() && line.enabled) {
			Vector3 linePos= parentLeaf.transform.position;
			linePos.x = linePos.x + (parentLeafPos * 1 - (level /2) -0.5f);
			linePos.y -= 0.5f;
			line.SetPosition (1, linePos);
		}
			
	}

	public void create(int _level) {
		keys = new List<int> ();
		leaves = new List<BTreeLeafScript> (_level+1);
		level = _level;
		line.enabled = true;
	}

	/**
	 * 	INSERTION
	 */
	public IEnumerator insertKey(int newKey) {

		yield return (StartCoroutine(bTree.moveKey(transform.position, newKey, insertColor)));

		if (hasLeaves()) {
			int pos = findInsertPosition (newKey);
			// ASK LEAF TO INSERT
			if (leaves.Count > pos) {
				yield return StartCoroutine(highlightLeaf(leafColor));
				yield return StartCoroutine(leaves[pos].highlightLine());
				yield return StartCoroutine(leaves[pos].insertKey (newKey));
			// CREATE NEW LEAF
			} else {
				GameObject leafObj = Instantiate (bTree.prefab, transform.position, Quaternion.identity) as GameObject;
				BTreeLeafScript newLeaf= leafObj.GetComponent<BTreeLeafScript> ();
				newLeaf.create (level);
				addLeaf(newLeaf, pos);
				yield return StartCoroutine(newLeaf.insertKey(newKey));
			}

		// INSERT IN THIS LEAF
		} else {
			if (!isFull ()) {
				yield return StartCoroutine(highlightLeaf(leafColor));
				bTree.deleteKey();
				addKey (newKey);
				yield return StartCoroutine(highlightNumber(keys.IndexOf(newKey), insertColor));
			} else {
				yield return StartCoroutine(split(newKey, false));	
			}
		}
	}



	private IEnumerator split(int newKey, bool innerSplit) {
		yield return StartCoroutine(highlightLeaf(deleteColor));
		addKey (newKey);

		int median = keys[(keys.Count / 2)];
		removeKey (median);

		Vector3 parentPos= transform.position;
		// CREATE PARENT IF ROOT
		if (isRoot ()) {
			GameObject parentObj = Instantiate (bTree.prefab, transform.position, Quaternion.identity) as GameObject;
			parentLeaf = parentObj.GetComponent<BTreeLeafScript> ();
			parentLeaf.create (level);
			parentLeaf.line.enabled= false;
			bTree.root = parentLeaf;

			// ANIMATE
			parentPos.y += 3;
			parentLeaf.moveLinear(parentPos);
			yield return StartCoroutine(parentLeaf.coRoutineMoveLinear(10f, transform.position, parentPos));
		}

		// CREATE TWO LEAVES
		GameObject leftObj= Instantiate (bTree.prefab, transform.position, Quaternion.identity) as GameObject;
		BTreeLeafScript leftLeaf= leftObj.GetComponent<BTreeLeafScript> ();
		leftLeaf.create (level);

		GameObject rightObj= Instantiate (bTree.prefab, transform.position, Quaternion.identity) as GameObject;
		BTreeLeafScript rightLeaf= rightObj.GetComponent<BTreeLeafScript> ();
		rightLeaf.create (level);

		for (int i=0; i< keys.Count; i++) { 
			if (keys[i] < median) {
				leftLeaf.addKey(keys[i]);
			} else {
				rightLeaf.addKey(keys[i]);
			}
		}

		int j=0;
		bool addLastLeaf = false;
		for (int i=0; i< leaves.Count; i++) {
			if (keys.Count > i && keys[i] < median) {
				leftLeaf.addLeaf(leaves[i], i);
			} else if (innerSplit) {
				Debug.Log ("Inner Split");
				leftLeaf.addLeaf (leaves[i], i);
				innerSplit= false;
			} else {
				rightLeaf.addLeaf(leaves[i], j);
				j++;
			}
		}


		parentLeaf.removeLeaf (this);

		/**
		 *  RIDICULOUS HIDE BEFORE DESTORY FOR SCRIPT TO CONTINUE
		 */ 
		bTree.addRemoveMeLeaf (this);

		yield return StartCoroutine(parentLeaf.propagateKey (median, leftLeaf, rightLeaf));

	}

	/**
	 *	Return new position 
	 */
	public IEnumerator propagateKey(int newKey, BTreeLeafScript leftLeaf, BTreeLeafScript rightLeaf) {
		int pos= findInsertPosition(newKey);

		this.addLeaf (leftLeaf, pos);
		this.addLeaf (rightLeaf, pos + 1);

		for (int i=pos + 2; i< leaves.Count; i++) {
			leaves[i].parentLeafPos += 1;
		}


		/**
		 * 	ANIMATIONS
		 */
		
		// ANIMATE PREVIOUS LEAVES
		foreach (BTreeLeafScript leaf in leftLeaf.getLeftSiblings ()) {
			Vector3 newPreviousPos= leaf.transform.position;
			newPreviousPos.x -= ((level / 2) + 1);
			leaf.moveLinear(newPreviousPos);
		}

		
		// ANIMATE NEW LEAVES
		Vector3 newLPos= leftLeaf.transform.position;
		newLPos.x -= (float) (level - 1);
		leftLeaf.renderer.material.color = Color.green;
		leftLeaf.moveLinear(newLPos);
		
		Vector3 newRPos= rightLeaf.transform.position;
		newRPos.x += (float) (level - 1);
		rightLeaf.renderer.material.color = Color.green;
		rightLeaf.moveLinear(newRPos);
		
		// ANIMATE NEXT LEAVES
		foreach (BTreeLeafScript leaf in rightLeaf.getRightSiblings ()) {
			Vector3 newPreviousPos= leaf.transform.position;
			newPreviousPos.x += ((level / 2) + 1);
			leaf.moveLinear(newPreviousPos);
		}

		/**
		 * 	END - ANIMATIONS
		 */ 
		
		yield return StartCoroutine(bTree.changeKey(newKey));
		yield return StartCoroutine(bTree.moveKey(transform.position, newKey, insertColor));

		if (!isFull ()) {
			yield return StartCoroutine(highlightLeaf(leafColor));
			bTree.deleteKey();
			addKey (newKey);
			yield return StartCoroutine(highlightNumber(keys.IndexOf(newKey), insertColor));
		}
		else {
			yield return StartCoroutine(split(newKey, true));
		}
	}

	/**
	 *	DELETION 
	 */
	public IEnumerator deleteKey(int newKey) {

		yield return (StartCoroutine(bTree.moveKey(transform.position, newKey, deleteColor)));
		yield return (StartCoroutine(highlightLeaf(leafColor)));

		int i;
		bool positionFound = false;
		for (i=0; i< keys.Count; i++) { 
			if (newKey <= keys[i]) {
				positionFound= true;
				break;
			}
		}

		// Found position, either key or leaf position
		if (positionFound) {
			if (newKey == keys [i]) {

				// Animate
				bTree.deleteKey();
				yield return (StartCoroutine(highlightNumber(keys.IndexOf(newKey), deleteColor)));
				removeKeyAt (i);
				// End-Animate

				yield return StartCoroutine(checkRemoval(i));
			} else {
				if (leaves.Count >= i + 1) {
					yield return (StartCoroutine(leaves [i].highlightLine()));
					yield return (StartCoroutine(leaves [i].deleteKey (newKey)));
				}
				else {
					Debug.Log ("Didn't found key: " + newKey);
					bTree.deleteKey();
				}
			}
		}

		// Position in Last leaf or nowhere
		else {
			if (leaves.Count >= i + 1) {
				yield return (StartCoroutine(leaves [i].highlightLine()));
				yield return (StartCoroutine(leaves [i].deleteKey (newKey)));
			}
			else {
				Debug.Log ("Didn't found key: " + newKey);
				bTree.deleteKey();
				return false;
			}

		}
	
	}

	public IEnumerator checkRemoval(int pos) {
		Debug.Log ("check removal start");
		//
		if (hasLeaves ()) {
			yield return StartCoroutine (chooseNewSeperator (pos));
		} else {
			yield return StartCoroutine (rebalanceDeletion ());
		}

		Debug.Log ("check removal returns");
	}

	public IEnumerator chooseNewSeperator(int pos) {
		// check left leaf get largest element	(last)
		// Animate Set New Seperator 
		BTreeLeafScript lastRightLeaf= leaves[pos].getLastRightLeaf ();
		int newKey= lastRightLeaf.getLastKey ();

		Debug.Log ("Choose new seperator");
		yield return (StartCoroutine(lastRightLeaf.highlightNumber(lastRightLeaf.keys.IndexOf(newKey), deleteColor)));
		lastRightLeaf.removeKey (newKey);
		yield return (StartCoroutine(bTree.moveKey(lastRightLeaf.transform.position, newKey, insertColor)));
		yield return (StartCoroutine(bTree.moveKey(transform.position, newKey, insertColor)));
		yield return (StartCoroutine(bTree.highlightKey(insertColor)));

		addKey (newKey);
		bTree.deleteKey();
		yield return StartCoroutine(highlightNumber(keys.IndexOf(newKey), insertColor));

		yield return StartCoroutine (lastRightLeaf.rebalanceDeletion ());

		// check righ leaf get smallest element (first)
		
		Debug.Log ("Choose new seperator returns");
	}

	public IEnumerator rebalanceDeletion() {
		Debug.Log ("Rebalance start");
		if (isEmpty ()) {
			if (isRoot ()) {
				// Animate Deletion
				yield return StartCoroutine(highlightLeaf(deleteColor));

				bTree.addRemoveMeLeaf(this);
				bTree.root= leaves[0];
				leaves[0].line.enabled= false;
				Debug.Log ("I am Root and i will be rebalanced");
			}
			else {
				// Rotate Left (Check Right Leaf)
				if (parentLeaf.checkLeftRotation (parentLeafPos)) {
					yield return StartCoroutine(rotateLeft());
				// Rotate Right (Check Left Leaf)
				} else if (parentLeaf.checkRightRotation (parentLeafPos)) {
					yield return StartCoroutine(rotateRight ());
				// Merge Siblings
				} else {
					yield return StartCoroutine(merge());
					yield return StartCoroutine(parentLeaf.rebalanceDeletion());
				// Rebalance?
				}

			}// ELSE (NOT ROOT)
		}// IF EMPTY

		Debug.Log ("Rebalance returns");
	}

	public IEnumerator rotateLeft() {
		BTreeLeafScript rightLeaf = parentLeaf.leaves [parentLeafPos + 1];

		// Animate Rebalancing
		Debug.Log ("Rotate Left");
		StartCoroutine(highlightLeaf(leafColor, false));
		StartCoroutine(rightLeaf.highlightLeaf(leafColor, false));
		yield return StartCoroutine(parentLeaf.highlightLeaf(leafColor, false));	
	

		// Rotate Key + Animate Rotation
		int seperator = parentLeaf.keys [getSeperatorPos()];
		
		yield return (StartCoroutine(parentLeaf.highlightNumber(parentLeaf.keys.IndexOf(seperator), deleteColor)));
		parentLeaf.removeKey (seperator);
		yield return (StartCoroutine(bTree.moveKey(parentLeaf.transform.position, seperator, insertColor)));
		yield return (StartCoroutine(bTree.moveKey(transform.position, seperator, insertColor)));
		yield return (StartCoroutine(bTree.highlightKey(insertColor)));
		addKey (seperator);
		bTree.deleteKey();
		yield return StartCoroutine(highlightNumber(keys.IndexOf(seperator), insertColor));

		int firstKey = rightLeaf.keys [0];
		yield return (StartCoroutine(rightLeaf.highlightNumber(rightLeaf.keys.IndexOf(firstKey), deleteColor)));
		rightLeaf.removeKeyAt (0);
		yield return (StartCoroutine(bTree.moveKey(rightLeaf.transform.position, firstKey, insertColor)));
		yield return (StartCoroutine(bTree.moveKey(parentLeaf.transform.position, firstKey, insertColor)));
		yield return (StartCoroutine(bTree.highlightKey(insertColor)));
		parentLeaf.addKey (firstKey);
		bTree.deleteKey();
		yield return StartCoroutine(parentLeaf.highlightNumber(parentLeaf.keys.IndexOf(firstKey), insertColor));

		
		// Rotate Leaf
		if (rightLeaf.hasLeaves ()) {
			BTreeLeafScript leafToMove= rightLeaf.leaves[0];
			rightLeaf.removeLeaf(leafToMove);
			foreach(BTreeLeafScript leaf in rightLeaf.leaves) {
				leaf.parentLeafPos -= 1;
			}
			addLeaf (leafToMove, leaves.Count);
		}

		// Animate Rebalancing
		Debug.Log ("Rotate Left");
		StartCoroutine(highlightLeaf(leafColor));
		StartCoroutine(rightLeaf.highlightLeaf(leafColor));
		yield return StartCoroutine(parentLeaf.highlightLeaf(leafColor));	
		
	}

	public IEnumerator rotateRight() {
		BTreeLeafScript leftLeaf = parentLeaf.leaves [parentLeafPos - 1];

		// Animate Rebalancing
		Debug.Log ("Rotate Right");
		StartCoroutine(highlightLeaf(leafColor, false));
		StartCoroutine(leftLeaf.highlightLeaf(leafColor, false));
		yield return StartCoroutine(parentLeaf.highlightLeaf(leafColor, false));	

		// Rotate Key + Animate Rotation
		int seperator = parentLeaf.keys [leftLeaf.getSeperatorPos()];
		
		yield return (StartCoroutine(parentLeaf.highlightNumber(parentLeaf.keys.IndexOf(seperator), deleteColor)));
		parentLeaf.removeKey (seperator);
		yield return (StartCoroutine(bTree.moveKey(parentLeaf.transform.position, seperator, insertColor)));
		yield return (StartCoroutine(bTree.moveKey(transform.position, seperator, insertColor)));
		yield return (StartCoroutine(bTree.highlightKey(insertColor)));
		addKey (seperator);
		bTree.deleteKey();
		yield return StartCoroutine(highlightNumber(keys.IndexOf(seperator), insertColor));
		
		int lastKey = leftLeaf.getLastKey ();
		yield return (StartCoroutine(leftLeaf.highlightNumber(leftLeaf.keys.IndexOf(lastKey), deleteColor)));
		leftLeaf.removeKeyAt(leftLeaf.keys.Count - 1);
		yield return (StartCoroutine(bTree.moveKey(leftLeaf.transform.position, lastKey, insertColor)));
		yield return (StartCoroutine(bTree.moveKey(parentLeaf.transform.position, lastKey, insertColor)));
		yield return (StartCoroutine(bTree.highlightKey(insertColor)));
		parentLeaf.addKey (lastKey);
		bTree.deleteKey();
		yield return StartCoroutine(parentLeaf.highlightNumber(parentLeaf.keys.IndexOf(lastKey), insertColor));

		// Rotate Leaf
		if (leftLeaf.hasLeaves ()) {
			BTreeLeafScript leafToMove= leftLeaf.leaves[leftLeaf.leaves.Count - 1];
			leftLeaf.removeLeaf(leafToMove);
			foreach(BTreeLeafScript leaf in leaves) {
				leaf.parentLeafPos += 1;
			}
			addLeaf (leafToMove, 0);
		}

		// Animate Rebalancing
		Debug.Log ("Rotate Right");
		StartCoroutine(highlightLeaf(leafColor));
		StartCoroutine(leftLeaf.highlightLeaf(leafColor));
		yield return StartCoroutine(parentLeaf.highlightLeaf(leafColor));
	}
	
	public IEnumerator merge() {
		BTreeLeafScript mergeLeftLeaf;
		BTreeLeafScript mergeRightLeaf;
		if (isLastLeaf ()) {
			
			mergeLeftLeaf = parentLeaf.leaves [parentLeafPos - 1];
			mergeRightLeaf= this;
		} else {
			
			mergeLeftLeaf= this;
			mergeRightLeaf= parentLeaf.leaves [parentLeafPos + 1];
		}

		// Animate Rebalancing
		Debug.Log ("Merge siblings");
		StartCoroutine(mergeLeftLeaf.highlightLeaf(leafColor, false));
		StartCoroutine(mergeRightLeaf.highlightLeaf(leafColor, false));
		yield return StartCoroutine(parentLeaf.highlightLeaf(leafColor, false));	

		int seperator = parentLeaf.keys [getSeperatorPos()];
		yield return StartCoroutine(parentLeaf.highlightNumber(parentLeaf.keys.IndexOf(seperator), deleteColor));
		parentLeaf.removeSeparator (getSeperatorPos());
		yield return StartCoroutine(bTree.moveKey(parentLeaf.transform.position, seperator, insertColor));
		yield return StartCoroutine(bTree.moveKey(mergeLeftLeaf.transform.position, seperator, insertColor));
		yield return StartCoroutine(bTree.highlightKey(insertColor));
		mergeLeftLeaf.addKey(seperator);
		bTree.deleteKey();
		yield return StartCoroutine(mergeLeftLeaf.highlightNumber(mergeLeftLeaf.keys.IndexOf(seperator), insertColor));
		
		foreach (int key in mergeRightLeaf.keys) {
		
			yield return (StartCoroutine(mergeRightLeaf.highlightNumber(mergeRightLeaf.keys.IndexOf(key), deleteColor)));
			yield return (StartCoroutine(bTree.moveKey(mergeRightLeaf.transform.position, key, insertColor)));
			yield return (StartCoroutine(bTree.moveKey(mergeLeftLeaf.transform.position, key, insertColor)));
			yield return (StartCoroutine(bTree.highlightKey(insertColor)));
			mergeLeftLeaf.addKey(key);
			bTree.deleteKey();
			yield return StartCoroutine(mergeLeftLeaf.highlightNumber(mergeLeftLeaf.keys.IndexOf(key), insertColor));
		}

		foreach (BTreeLeafScript leaf in mergeRightLeaf.leaves) {
			//mergeRightLeaf.removeKey(key);
			mergeLeftLeaf.addLeaf(leaf, mergeLeftLeaf.leaves.Count);
		}

		// DELETE RIGHT LEAF
		yield return StartCoroutine(mergeRightLeaf.highlightLeaf(deleteColor));	
		mergeRightLeaf.parentLeaf.removeLeaf (mergeRightLeaf);


		bTree.addRemoveMeLeaf (mergeRightLeaf);

		/**
		 * 	MOVEMENT ANIMATIONS
		 */
		// ANIMATE PREVIOUS LEAVES
		foreach (BTreeLeafScript leaf in mergeLeftLeaf.getLeftSiblings ()) {
			Vector3 newPreviousPos= leaf.transform.position;
			newPreviousPos.x += ((level / 2) + 1);
			leaf.moveLinear(newPreviousPos);
		}

		// ANIMATE DELETION LEAVES
		Vector3 newLPos= mergeLeftLeaf.transform.position;
		newLPos.x += (float)(level / 2) + 1;
		mergeLeftLeaf.moveLinear(newLPos);
		
		// ANIMATE NEXT LEAVES
		foreach (BTreeLeafScript leaf in mergeLeftLeaf.getRightSiblings ()) {
			Vector3 newPreviousPos= leaf.transform.position;
			newPreviousPos.x -= 1;
			leaf.moveLinear(newPreviousPos);
		}
		
		/**
		 * 	END - MOVEMENT ANIMATIONS
		 */ 

		// Animate Rebalancing
		mergeLeftLeaf.StartCoroutine(mergeLeftLeaf.highlightLeaf(leafColor));
		yield return StartCoroutine (parentLeaf.highlightLeaf (leafColor));

		Debug.Log ("Merge returns");
	}


	/**
	 * 	GRAPHICS / ANIMATIONS
	 */

	private void showKeys() {
		int counter = 0;
		foreach (Transform child in transform) {
			if (keys.Count > counter)
				child.GetChild (0).GetComponent<TextMesh> ().text = keys [counter].ToString ();
			else 
				child.GetChild (0).GetComponent<TextMesh> ().text = "";

			counter ++;
		}
	}

	
	// FIXME fix this duplicate shit.
	public void moveLinear(Vector3 target) {
		
		float speed = 10.0f;
		StartCoroutine(coRoutineMoveLinear(speed, this.transform.position, target));
		//planetObject.audio.Play ()l
	}

	public IEnumerator coRoutineMoveLinear(float speed, Vector3 start, Vector3 target) {
		
		float startTime = Time.time;
		float journeyLength = Vector3.Distance(start, target);
		float fracJourney = 0;
		
		while (fracJourney <= 1) {
			
			// Distance moved = time * speed.
			var distCovered = (Time.time - startTime) * speed;
			
			// Fraction of journey completed = current distance divided by total distance.
			fracJourney = distCovered / journeyLength;
			
			// Set our position as a fraction of the distance between the markers.
			transform.position = Vector3.Lerp (start, target, fracJourney);
			
			yield return null;
		}
	}

	public IEnumerator highlightLeaf(Color color) {
		yield return StartCoroutine(highlightLeaf(color, true));
	}

	public IEnumerator highlightLeaf(Color color, bool remove) {
		gameObject.light.color= color;
		gameObject.light.renderer.light.range= 3;
		
		yield return new WaitForSeconds (bTree.waitSeconds);

		if (remove) gameObject.light.renderer.light.range= 0;
	}

	public IEnumerator highlightLine() {
		line.SetColors (lineHighlightColor, lineHighlightColor);
		line.SetWidth(lineWidthStart + 0.2f, lineWidthEnd + 0.1f);
		
		yield return new WaitForSeconds (bTree.waitSeconds);

		line.SetColors (defaultLineStartColor, defaultLineEndColor);
		line.SetWidth(lineWidthStart, lineWidthEnd);
	}

	public IEnumerator highlightNumber(int pos, Color color) {
		int counter = 0;
		foreach (Transform child in transform) {
			if (pos == counter)
				child.GetChild (0).GetComponent<TextMesh> ().color= color;

			counter ++;
		}
		
		yield return new WaitForSeconds (bTree.waitSeconds);

		foreach (Transform child in transform) {
			child.GetChild (0).GetComponent<TextMesh> ().color= defaultNumberColor;
		}
	}
		
	/**
	 * 	UTILITIES
	 */ 
	private void addKey(int newKey) {
		int pos = findInsertPosition (newKey);
		keys.Insert (pos, newKey);
		showKeys ();
	}
	
	private int removeKeyAt(int pos) {
		int key = keys [pos];
		keys.Remove (key);
		showKeys ();
		return key;
	}

	private void removeSeparator(int pos) {
		removeKeyAt (pos);
		for (int i= pos + 2; i < leaves.Count; i++) {
			leaves[i].parentLeafPos -= 1;
		}
	}
	
	private int removeKey(int newKey) {
		keys.Remove (newKey);
		showKeys ();

		return newKey;
	}
	
	private void removeLeaf(BTreeLeafScript leaf) {
		leaves.Remove (leaf);
	}
	
	private void addLeaf(BTreeLeafScript leaf, int parentPos) {
		if (leaves.Count < parentPos)
			leaves.Add (leaf);
		else
			leaves.Insert (parentPos, leaf);
		leaf.parentLeaf = this;
		leaf.parentLeafPos = parentPos;
		//TODO: showLeaves ();
	}
	
	private int findInsertPosition(int newKey) {
		for (int i=0; i< keys.Count; i++) { 
			if (newKey < keys[i]) return i;
		}
		return keys.Count;
	}

	public bool isRoot() {
		return parentLeaf == null;
	}

	public bool isFull() {
		return keys.Count >= level;
	}

	public bool isEmpty() {
		return keys.Count == 0;
	}

	public bool hasMinimumKeys() {
		return keys.Count <= 1;
	}

	public bool hasLeaves() {
		foreach (BTreeLeafScript leaf in leaves) {
			if (leaf != null) return true;
		}
		return false;
	}

	public int getLastKey() {
		return keys [keys.Count - 1];
	}

	public BTreeLeafScript getLastRightLeaf() {
		if (leaves.Count == 0)
			return this;
		else {
			return leaves[leaves.Count - 1].getLastRightLeaf();
		}
	}

	public bool checkLeftRotation(int parentPos) {
		return 
			!((parentPos + 1 >= leaves.Count) ||
			(leaves [parentPos + 1].hasMinimumKeys()));
	}

	public bool checkRightRotation(int parentPos) {
		return
			!((parentPos - 1 < 0) ||
			(leaves[parentPos - 1].hasMinimumKeys()));
	}

	public bool isFirstLeaf() {
		return parentLeafPos == 0;
	}

	public bool isLastLeaf() {
		return parentLeafPos + 1 == parentLeaf.leaves.Count;
	}

	public int getSeperatorPos() {
		int seperatorPos; 
		if (isLastLeaf ()) 
			seperatorPos = parentLeafPos - 1;
		else
			seperatorPos= parentLeafPos;
		return seperatorPos; 
	}

	// Disgusting 2nd Level depth siblings
	public List<BTreeLeafScript> getLeftSiblings() {
		List<BTreeLeafScript> resultList = new List<BTreeLeafScript> ();
		if (!isRoot()) {
			for(int i=0; i < parentLeafPos; i++) {
				resultList.Add(parentLeaf.leaves[i]);
			}

			if (!parentLeaf.isRoot()) {
				for(int i= 0; i < parentLeaf.parentLeafPos ; i++) {
					foreach(BTreeLeafScript innerLeaf in parentLeaf.parentLeaf.leaves[i].leaves) {
						resultList.Add (innerLeaf);
					}
				}
			}
		}

		return resultList;
	}

	// Disgusting 2nd Level depth siblings
	public List<BTreeLeafScript> getRightSiblings() {
		List<BTreeLeafScript> resultList = new List<BTreeLeafScript> ();
		if (!isRoot()) {
			for(int i= parentLeafPos + 1; i < parentLeaf.leaves.Count; i++) {
					resultList.Add(parentLeaf.leaves[i]);
			}
			
			if (!parentLeaf.isRoot()) {
				for(int i= parentLeaf.parentLeafPos + 1; i < parentLeaf.parentLeaf.leaves.Count; i++) {
					foreach(BTreeLeafScript innerLeaf in parentLeaf.parentLeaf.leaves[i].leaves) {
						resultList.Add (innerLeaf);
					}
				}
			}
		}
		return resultList;
	}
}
