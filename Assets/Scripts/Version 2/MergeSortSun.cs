using UnityEngine;
using System;
using System.Collections.Generic;

public class MergeSortSun : Sun {

	List<Vector3> targetPositions= new List<Vector3>();

	public override string getPseudoCode () {
		return "function merge_sort(list m)\n" + 
			"/* Base case. A list of zero or one elements is sorted, by definition. */\n" + 
				"if length(m) <= 1\n" + 
				"    return m\n\n" + 
				"/* Recursive case. First, *divide* the list into equal-sized sublists. */\n" + 
				"var list left, right\n" + 
				"var integer middle = length(m) / 2\n" + 
				"for each x in m before middle\n" + 
				"     add x to left\n" + 
				"for each x in m after or equal middle\n" + 
				"     add x to right\n\n" + 
				"/* Recursively sort both sublists */\n" + 
				"left = merge_sort(left)\n" + 
				"right = merge_sort(right)\n\n" + 
				"/* Then merge the now-sorted sublists. */\n" + 
				"return merge(left, right)";
	}

	public override void run() {
		foreach (Planet p in base.getPlanetList ()) {
			targetPositions.Add(p.transform.position);
		}
		generateMergeSortList (0, getPlanetList());
	}
	
	private List<Planet> generateMergeSortList(int startingPos, List<Planet> currentList) {
		int middle= 0;
		List<Planet> currentPlanetList = new List<Planet> ();
		currentPlanetList.AddRange (currentList);

		List<Planet> leftList = new List<Planet> ();
		List<Planet> rightList = new List<Planet> ();
		
		getStepList ().add (new MergeNewListStep (currentList));

		if (currentPlanetList.Count > 1) {
			// Find Middle
			middle = currentPlanetList.Count / 2;

			// STEP SPLIT?

			// Loop List
			for (int i=0; i< middle; i++) {
					leftList.Add (currentPlanetList [i]);
			}	
			for (int i=middle; i< currentPlanetList.Count; i++) {
					rightList.Add (currentPlanetList [i]);
			}

			//rightList.Reverse ();

			return merge (startingPos, generateMergeSortList (startingPos, leftList), generateMergeSortList (startingPos + middle, rightList));
		} else {	
			return currentPlanetList;
		}
				
	}
	
	private List<Planet> merge(int startingPos, List<Planet> leftList, List<Planet> rightList) {
		List<Planet> mergedList = new List<Planet> ();
		int nextPosition= startingPos;
		int maxPositions = leftList.Count + rightList.Count;
		int counter = 0;

		getStepList ().add (new MergeStartStep (leftList, rightList));

		while (leftList.Count != 0 || rightList.Count != 0) {

			getStepList ().add (new MergeCompareStep (leftList, rightList, nextPosition, targetPositions[nextPosition]));
			mergedList.Add (MergeCompareStep.mergePlanets (ref leftList, ref rightList));
			nextPosition++;
			counter++;
		}

		return mergedList;
	}
	
}