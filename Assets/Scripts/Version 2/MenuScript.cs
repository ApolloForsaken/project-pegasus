using UnityEngine;
using System.Collections.Generic;

public class MenuScript : MonoBehaviour {

	void OnGUI() {
		
		if (GUI.Button (new Rect (10, 100, 100, 30), "Sort Level")) {
			Application.LoadLevel("sort");
		}

		if (GUI.Button (new Rect (10, 150, 100, 30), "Node List Level")) {
			Application.LoadLevel("node-list");
		}

		if (GUI.Button (new Rect (10, 200, 100, 30), "B-Tree Level")) {
			Application.LoadLevel("b-tree");
		}
	}
}