﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class QuickSortSun : Sun {

	public override void run() {
		generateQuickSortList (0, getPlanetList());
	}

	public override string getPseudoCode () {
		return "/* lo is the index of the leftmost element of the subarray*/\n" +
				"/* hi is the index of the rightmost element of the subarray (inclusive)*/\n" +
				"partition(A, lo, hi)\n" +
				"	pivotIndex := choosePivot(A, lo, hi)\n" +
				"		pivotValue := A[pivotIndex]\n" +
				"		/* put the chosen pivot at A[hi]\n" +
				"		swap A[pivotIndex] and A[hi]\n" +
				"		storeIndex := lo\n" +
				"		/* Compare remaining array elements against pivotValue = A[hi]*/\n" +
				"		for i from lo to hi−1, inclusive\n" +
				"			if A[i] < pivotValue\n" +
				"				swap A[i] and A[storeIndex]\n" +
				"				storeIndex := storeIndex + 1\n" +
				"					swap A[storeIndex] and A[hi]  /*Move pivot to its final place*/\n" +
				"					return storeIndex\n";
	}

	private void generateQuickSortList(int startingPos, List<Planet> currentList) {
		int pivot = 0;
		List<Planet> currentPlanetList = new List<Planet> ();
		currentPlanetList.AddRange (currentList);
	
		List<Planet> leftList = new List<Planet> ();
		List<Planet> rightList = new List<Planet> ();

		getStepList ().add (new QSNewListStep (currentList));

		if (currentPlanetList.Count > 1) {
			// Find Pivot
			pivot = currentPlanetList.Count - 1;

			getStepList ().add (new QSPivotStep (currentList[pivot]));

			// Loop List
			int i = 0;
			while (i < currentPlanetList.Count - 1) {

				if (currentPlanetList [i].transform.localScale.x > currentPlanetList [pivot].transform.localScale.x) {

					rightList.Add (currentPlanetList [i]);

					if (Math.Abs (pivot - i) == 1) {
						getStepList ().add (new QSCompareStep (i + startingPos, pivot + startingPos, true));
						QSCompareStep.swapPlanets (ref currentPlanetList, i, pivot);
				
					} else {	
						getStepList ().add (new QSCompareStep (i + startingPos, pivot + startingPos, true));
						QSCompareStep.swapPlanets (ref currentPlanetList, i, pivot);
					}
					pivot--;

					currentPlanetList.	RemoveAt (currentPlanetList.Count - 1);

				} else {
					leftList.Add (currentPlanetList [i]);

					getStepList ().add (new QSCompareStep (i + startingPos, pivot + startingPos, false));
					i++;
				}
			}	

		// Right List should be after pivot (not include it)
		pivot++;
		
		generateQuickSortList (startingPos, leftList);

		rightList.Reverse ();
		generateQuickSortList (startingPos + pivot, rightList);
		}
	}

	// TODO: move to SUN
//	void OnGUI() {
//		String algorithm = "Quicksort() {" +
//			"}";
//
//		GUI.Box (new Rect (10, 150, 100, 30), "nothing");
//	}
}