
using UnityEngine;
using System;
using System.Collections.Generic;

public class ShellSortScript : Sun {

	// Marcin Ciura's gap sequence, 
	private List<int> gaps= new List<int> {701, 301, 132, 57, 23, 10, 4, 1};
	private bool swapped;

	public override void run() {
		generateShellSortList (getPlanetList());
	}

	public override string getPseudoCode () {
		return "function merge_sort(list m)" + 
			"/* Base case. A list of zero or one elements is sorted, by definition. */" + 
				"if length(m) <= 1" + 
				"    return m" + 
				"\n" + 
				"/* Recursive case. First, *divide* the list into equal-sized sublists. */" + 
				"var list left, right" + 
				"var integer middle = length(m) / 2" + 
				"for each x in m before middle" + 
				"     add x to left" + 
				"for each x in m after or equal middle" + 
				"     add x to right" + 
				"\n" + 
				"/* Recursively sort both sublists */" + 
				"left = merge_sort(left)" + 
				"right = merge_sort(right)" + 
				"\n" + 
				"/* Then merge the now-sorted sublists. */" + 
				"return merge(left, right)";
	}
	
	private void generateShellSortList( List<Planet> currentList) {
		
		List<Planet> currentPlanetList = new List<Planet> ();
		currentPlanetList.AddRange (currentList);

	
//		foreach (int gap in gaps) {
//			//getStepList ().add (new ShellGapStep (gap));
//
//			for (int i= gap; i< currentPlanetList.Count-1; i++) {
//				// add ai to the elements that have been gap sorted
//				// save ai in temp and make a hole at position i
//				int temp = currentPlanetList[i];
//				// shift earlier gap-sorted elements up until the correct location for a[i] is found
//				for (int j= i; j >= gap && currentPlanetList[j - gap] > temp; j -= gap) {
//					currentPlanetList[j] = currentPlanetList[j - gap];
//				}
//				// put temp (the original a[i]) in its correct location
//				currentPlanetList[j] = temp;
//
//				bool currentSwapped= currentPlanetList [i].getNumber() > currentPlanetList [i + 1].getNumber();
//				
//				//getStepList ().add (new ShellCompareStep (i, i+1, currentSwapped));
//				
//				if (currentSwapped){
//				//	ShellCompareStep.swapPlanets (ref currentPlanetList, i, i+1);
//					swapped = true;
//				}
//			}
//		} // END FOREACH GAP
		
	}
}