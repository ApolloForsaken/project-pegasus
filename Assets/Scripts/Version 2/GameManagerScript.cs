using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManagerScript : MonoBehaviour {

	public static GameObject inputField;
	public static GameObject outputField;
	public static GameObject playPanel;
	public static GameObject editPanel;

	public static AlgoCodeScript algoCode;
	public static ConsoleScript console;
	public static MainCameraScript mainCameraScript;

	private Sun activeSun;
	public static GameObject blackHole;
	public static NodeList nodeScript;
	public static GameObject nodePanel;
	public static GameObject chooseSortPanel;
	
	public static GameObject stackHole;
	public static Stack stackScript;
	public static GameObject stackPanel;

	public static GameObject queueHole;
	public static Queue queueScript;
	public static GameObject queuePanel;

	public static GameObject bTree;
	public static TreeScript treeScript;
	public static GameObject treePanel;

	public static GameObject topMenu;
	public GameObject activeBtn;

	public Color defaultBtnClr;// =  new Color((float)0.2,(float)0.16,(float)0.99,(float)0.5);
	public Color activeBtnClr;// = new Color((float)(0.6),(float)(0.24),(float)(0.5),(float)0.5);

	// Ridiculous way of last minute.
	public List<Material> unityMaterials;
	public static List<Material> materials;

	//private static Color activeBtnClr = Color.red;


	// Use this for initialization
	void Awake () {
		materials = unityMaterials;
		inputField= GameObject.Find("InputField");
		outputField= GameObject.Find("OutputField");
		playPanel= GameObject.Find("PlayPanel");
		editPanel= GameObject.Find("EditPanel");

		blackHole= GameObject.Find ("BlackHole");
		nodeScript= blackHole.GetComponentInChildren<NodeList>();
		nodePanel= GameObject.Find ("NodePanel");
		chooseSortPanel= GameObject.Find ("ChooseSortPanel");

		stackHole= GameObject.Find ("StackHole");
		stackScript= stackHole.GetComponentInChildren<Stack>();
		stackPanel= GameObject.Find ("StackPanel");

		queueHole= GameObject.Find ("QueueHole");
		queueScript= queueHole.GetComponentInChildren<Queue>();
		queuePanel= GameObject.Find ("QueuePanel");

		bTree= GameObject.Find ("B-Tree");
		treeScript= bTree.GetComponentInChildren<TreeScript>();
		treePanel= GameObject.Find ("TreePanel");

		mainCameraScript = Camera.main.gameObject.GetComponent<MainCameraScript>();
		algoCode= playPanel.GetComponentInChildren<AlgoCodeScript>();
		console= GameObject.Find("ConsolePanel").GetComponentInChildren<ConsoleScript>();
	
		topMenu = GameObject.Find("TopMenu");
	}

	void Start () {
		disablePanels ();
	}

	//
	//	### SORT PLAY PANEL ###
	//

	public void handlePlayPanelRun() {
		playPanel.transform.FindChild("RunButton").GetComponent<Button>().interactable= false;
		playPanel.transform.FindChild("NextButton").GetComponent<Button>().interactable= false;
		playPanel.transform.FindChild("PrevButton").GetComponent<Button>().interactable= false;
		activeSun.runStepList ();
	}	

	public void handlePlayPanelStop() {
		activeSun.stopStepList ();
		playPanel.transform.FindChild("RunButton").GetComponent<Button>().interactable= true;
	}

	public void handlePlayPanelNext() {
		activeSun.runNextStep ();
	}

	public void handlePlayPanelPrevious() {
		activeSun.runPreviousStep ();
	}

	public void handlePlayPanelBack() {
		StartCoroutine(FocusGalaxy(blackHole,nodePanel ,activeBtn));
	}
	//
	//	### NODE PANEL ###
	//
	
	public void handleNodeInput(string value) {
		if (value != "") {
			int intValue= int.Parse(value);
			Node current= nodeScript.insert(intValue);
			nodePanel.transform.FindChild("InputField").GetComponent<InputField>().text= "";
		}
	}
	
//	public void handleNodePanelShowGalaxy() {
//		mainCameraScript.focusGalaxy (blackHole.transform);
//	}
	
	public void handleNodePanelFirst() {
		nodeScript.getFirst();
	}

	public void handleNodePanelNext() {
		nodeScript.getNext();
	}

	public void handleNodePanelPrevious() {
		nodeScript.getPrevious();
	}


	public void handleNodePanelDelete() {
		Node current= nodeScript.delete();
	}

	
	public void handleNodePanelRun() {
		nodeScript.run ();
	}

	public void handleNodePanelStop() {
		nodeScript.stop ();
	}

	public void handleNodePanelChooseSort() {
		disablePanels ();
		chooseSortPanel.SetActive(true);
	}

	
	//
	//	### CHOOSE SORT PANEL ###
	//

	public void handleChooseBubble() {
		activeSun = GameObject.Find ("BubbleSortSun").GetComponent<Sun> ();
		mainCameraScript.focusSun (activeSun.transform);
		algoCode.setLine (1, "Scan List");
		algoCode.setLine (2, "Swap");
		algoCode.setLine (3, "Don't Swap");
		algoCode.setLine (4, "");
		algoCode.transform.FindChild("Description").GetComponent<Text>().text= "Bubble Sort";

		activeSun.reset ();
		List<int> numbers = nodeScript.getListNumbers ();
		foreach (int number in numbers) {
			activeSun.generatePlanet(number);
		}
		activeSun.run ();
		console.copy (activeSun.getPseudoCode(), 12);

		StartCoroutine (FocusSun ());
	}

	public void handleChooseMerge() {
		activeSun= GameObject.Find("MergeSortSun").GetComponent<Sun>();
		mainCameraScript.focusSun (activeSun.transform);
		Debug.Log ("Chose Merge");
		algoCode.setLine(1, "New List");
		algoCode.setLine(2, "Merge Lists");
		algoCode.setLine(3, "Copy left element");
		algoCode.setLine(4, "Copy right element");
		algoCode.transform.FindChild("Description").GetComponent<Text>().text= "Merge Sort";
	
		activeSun.reset ();
		List<int> numbers = nodeScript.getListNumbers ();
		foreach (int number in numbers) {
			activeSun.generatePlanet(number);
		}
		activeSun.run ();
		console.copy (activeSun.getPseudoCode(), 12);

		StartCoroutine (FocusSun ());
	}
		
	public void handleChooseQuick() {
		activeSun= GameObject.Find ("QuickSortSun").GetComponent<Sun>();
		mainCameraScript.focusSun (activeSun.transform);
		algoCode.setLine(1, "New List");
		algoCode.setLine(2, "Set Pivot");
		algoCode.setLine(3, "Swap");
		algoCode.setLine(4, "Don't Swap");
		algoCode.transform.FindChild("Description").GetComponent<Text>().text= "Quick Sort";
	
		activeSun.reset ();
		List<int> numbers = nodeScript.getListNumbers ();
		foreach (int number in numbers) {
			activeSun.generatePlanet(number);
		}
		activeSun.run ();
		console.copy (activeSun.getPseudoCode(), 12);

		StartCoroutine (FocusSun ());
	}

	public void handleChooseBack() {
		chooseSortPanel.SetActive(false);
		nodePanel.SetActive(true);
	}
		
//	public void handleChooseShell() {
//		activeSun= GameObject.Find ("ShellSortSun").GetComponent<Sun>();
//		disablePanels ();
//		playPanel.SetActive(true);
//
//		List<int> numbers = nodeScript.getListNumbers ();
//		foreach (int number in numbers) {
//			activeSun.generatePlanet(number);
//		}
//		activeSun.run ();
//	}

	//
	//	### STACK PANEL ###
	//

	public void handleStackPanelPush(string value) {
		if (value != "") {
			int intValue= int.Parse(value);
			stackScript.push (intValue);
			stackPanel.transform.FindChild("InputField").GetComponent<InputField>().text= "";
		}
	}

	public void handleStackPanelPop() {
		stackScript.pop ();
	}

	//
	//	### QUEUE PANEL ###
	//

	public void handleQueuePanelEnqueue(string value) {
		if (value != "") {
			int intValue= int.Parse(value);
			queueScript.enqueue (intValue);
			queuePanel.transform.FindChild("InputField").GetComponent<InputField>().text= "";
		}
	}

	public void handleQueuePanelDequeue() {
		queueScript.dequeue ();
	}

	//
	//	### TREE PANEL ###
	//

	public void handleTreePanelInsert(string value) {
		if (value != "") {
			int intValue = int.Parse (value);
			StartCoroutine (treeScript.insert (intValue));
			treePanel.transform.FindChild("InsertField").GetComponent<InputField>().text= "";
		}
	}

	public void handleTreePanelDelete(string value) {
		if (value != "") {
			int intValue = int.Parse (value);
			StartCoroutine (treeScript.delete (intValue));
			treePanel.transform.FindChild("DeleteField").GetComponent<InputField>().text= "";
		}
	}

	//
	//	### TOP MENU PANEL ###
	//

	public void handleTopMenuNodeList() {
		//disablePanels();
		//nodePanel.SetActive(true);
		//mainCameraScript.focusGalaxy (blackHole.transform);

		activeBtn = GameObject.Find("ListBtn");
		console.reset ();
		StartCoroutine(FocusGalaxy(blackHole,nodePanel ,activeBtn));
	}

	public void handleTopMenuStack() {
	//	disablePanels();
	//	stackPanel.SetActive(true);
	//	mainCameraScript.focusHole (stackHole.transform);
		activeBtn = GameObject.Find("StackBtn");
		console.reset ();
		StartCoroutine(FocusHole(stackHole,stackPanel ,activeBtn));
	}

	public void handleTopMenuQueue() {
	//	disablePanels ();
		//queuePanel.SetActive (true);
	//	mainCameraScript.focusHole (queueHole.transform);
		activeBtn = GameObject.Find("QueueBtn");
		console.reset ();
		StartCoroutine(FocusHole(queueHole,queuePanel,activeBtn));
	}

	public void handleTopMenuTree() {
	//	disablePanels ();
	//	treePanel.SetActive (true);
		activeBtn = GameObject.Find("TreeBtn");
		console.reset ();
		StartCoroutine(FocusHole(bTree,treePanel ,activeBtn));

		//mainCameraScript.focusHole (bTree.transform);
	}

	IEnumerator FocusHole(GameObject thing,GameObject panel,GameObject activeBtn) {
		disablePanels();
		handleTopButtons(activeBtn,false);
		yield return StartCoroutine(mainCameraScript.focusHole(thing.transform));
		handleTopButtons(activeBtn,true);
		panel.SetActive(true);

	}
	IEnumerator FocusGalaxy(GameObject thing,GameObject panel,GameObject activeBtn) {
		disablePanels();
		handleTopButtons(activeBtn,false);
		yield return StartCoroutine(mainCameraScript.focusGalaxy(thing.transform));
		handleTopButtons(activeBtn,true);
		panel.SetActive(true);
		
	}

	IEnumerator FocusSun() {
		disablePanels();
		activeBtn = GameObject.Find("ListBtn");
		handleTopButtons(activeBtn, false);
		yield return StartCoroutine(mainCameraScript.focusSun(activeSun.transform));
		handleTopButtons(activeBtn, true);
		playPanel.SetActive(true);
	}

	private void handleTopButtons(GameObject activeBtn ,bool active){
		 
		Color clr  =  (active)? defaultBtnClr : Color.grey;
		 
		foreach (Transform child in topMenu.transform){
			child.gameObject.GetComponent<Button>().enabled = active;
			if (child.name!=activeBtn.name){
				child.gameObject.GetComponent<Button>().image.color = clr;
			}else{
 				child.gameObject.GetComponent<Button>().enabled = false;
				child.gameObject.GetComponent<Button>().image.color = activeBtnClr;
					//new Color((float)(150/255),(float)(60/255),(float)(60/255),(float)0.5);
			}
		}
	}


	//
	//	### UTILITIES ###
	//
	private void disablePanels() {
		// TODO: FIX THIS (list)
		editPanel.SetActive(false);
		playPanel.SetActive(false);
		nodePanel.SetActive(false);
		chooseSortPanel.SetActive (false);
		stackPanel.SetActive (false);
		queuePanel.SetActive (false);
		treePanel.SetActive (false);
	}

}