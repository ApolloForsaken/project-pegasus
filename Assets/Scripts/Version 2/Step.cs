﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

abstract public class Step<T> where T : MonoBehaviour {

	protected static AlgoCodeScript algoCode= GameManagerScript.algoCode;
	protected static ConsoleScript console= GameManagerScript.console;

	abstract public void run(ref List<T> lst);
	abstract public void reverse(ref List<T> lst);
}