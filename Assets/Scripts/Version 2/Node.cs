﻿using UnityEngine;
using System.Collections;

public class Node {

	private Transform data;
	private Vector3 nextPosition;
	private Vector3 prevPosition;

	public Node(Transform _data) {
		data = _data;
	}

	public Vector3 getNext() {
		return nextPosition;
	}

	public Vector3 getPrev() {
		return prevPosition;
	}

	public Vector3 getPosition() {
		return data.transform.position;
	}

	public Planet getPlanet() {
		return data.GetComponent<Planet> ();
	}

	public void setNextPosition(Vector3 _nextPosition) {
		nextPosition = _nextPosition;
	}
	
	public void setPrevPosition(Vector3 _prevPosition) {
		prevPosition = _prevPosition;
	}
	
}
