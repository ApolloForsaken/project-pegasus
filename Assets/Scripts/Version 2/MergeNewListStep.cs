﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
//using System.Text.StringBuilder;

public class MergeNewListStep : Step<Planet> {

	private List<Planet> newList;
	
	public MergeNewListStep(List<Planet> newList) {
		this.newList = newList;
	}
	
	public override void run(ref List<Planet> lst) {
	
		// TODO: put to Sun (or Planets Handler ... whatever) Highlight Planets
		foreach (Planet go in lst) {
			go.highlight (false);
		}

		string planetList = "";
		
		foreach(Planet go in newList) {
			go.highlight(true);
			planetList+= go.number;
			planetList+= " ,";
		}

		planetList= planetList.Remove (planetList.Length - 1);

		//AlgoCodeScript algoCode= GameObject.Find ("AlgoCode").GetComponent<AlgoCodeScript>();
		algoCode.replaceLine(1, "New List: " + planetList);
//		console.consoleWrite ("MergeSort( " + planetList + " )");
	}

	public override void reverse(ref List<Planet> planetList) {
		foreach (Planet go in planetList) {
			go.highlight (false);
		}
	}
}
