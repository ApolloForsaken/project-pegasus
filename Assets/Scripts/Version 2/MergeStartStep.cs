﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MergeStartStep : Step<Planet> {
	
	private List<Planet> leftList= new List<Planet> ();
	private List<Planet> rightList= new List<Planet> ();
	private Vector3 targetPos;
	private String lnames;
	private String rnames;
	
	public MergeStartStep(List<Planet> leftList, List<Planet> rightList) {

		this.leftList.AddRange(leftList);
		this.rightList.AddRange(rightList);

	}

	public override void run(ref List<Planet> lst) {

		foreach (Planet p in lst) {
			p.highlight (false);
		}

		lnames = "";
		rnames = "";

		foreach(Planet go in leftList) {
			targetPos= go.transform.position;
			targetPos.x -= 2;
			targetPos.z -= 2;
			go.move (targetPos, Vector3.down);
			go.highlight (true);
			lnames+= go.number + " ,";
		}

		foreach(Planet go in rightList) {
			targetPos= go.transform.position;
			targetPos.x += 2;
			targetPos.z -= 2;
			go.move (targetPos, Vector3.down);
			go.highlight (true);
			rnames+= go.number + " ,";
		}

		lnames= lnames.Remove (lnames.Length - 1);
		rnames= rnames.Remove (rnames.Length - 1);

		algoCode.replaceLine (2, "Merge Lists: " + lnames + " with " + rnames);
	}

	public override void reverse(ref List<Planet> lst) {

		foreach(Planet go in leftList) {
			targetPos= go.transform.position;
			targetPos.x += 2;
			targetPos.z += 2;
			go.move (targetPos, Vector3.up);
			go.highlight (false);
		}
		
		foreach(Planet go in rightList) {
			targetPos= go.transform.position;
			targetPos.x -= 2;
			targetPos.z += 2;
			go.move (targetPos, Vector3.up);
			go.highlight (false);
		}
	}
}