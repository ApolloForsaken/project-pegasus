using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Page  : MonoBehaviour{

	public static Page root;
	public static GameObject prefab;

	public Page parent;
	public int level;
	public List<int> keys;
	public List<Page> leaves;
	private int attachedToParentKeyPos;
	private LineRenderer line;

	Dictionary<string, Transform> children;

	public void Awake() {
		children= new Dictionary<string, Transform>();
		line= GetComponent<LineRenderer> ();
		line.enabled = false;
	}

	public void Update() {
		
		line.SetPosition (0, transform.position);
		if(line.enabled= true && parent != null) {
			line.SetPosition (1, getPageLinePos(parent.transform.position));
		}
		// (fantastiko animation?)

	}

	public void enableLine(int parentKeyPos) {
		attachedToParentKeyPos= parentKeyPos;
		line.enabled= true;
	}

	public void setPage(Page parent, int level) {
		this.parent = parent;
		this.level = level;

		keys = new List<int>();
		leaves = new List<Page>();

		for (int i=0; i< getMaxLeaves(); i++) {
			leaves.Add(null);
		}

		prefab= ((TreeScript) FindObjectOfType(typeof(TreeScript))).prefab;
		foreach(Transform child in transform){
			children.Add(child.name, child);
		}
	}

	public void setPage(Page parent, int level, List<int> keys, List<Page> leaves) {
		this.parent = parent;
		this.level = level;

		prefab= ((TreeScript) FindObjectOfType(typeof(TreeScript))).prefab;
		foreach(Transform child in transform){
			children.Add(child.name, child);
		}


		foreach (int key in keys) {
			addKey(key);		
		}

		//this.keys= keys;
		this.leaves = leaves;

		for (int i=leaves.Count; i< getMaxLeaves(); i++) {
			leaves.Add(null);
		}
	}

	public void insertKey(int newKey) {


		// If this is not the last level of the tree propagate to next level leaf to insert
		if (hasLeaves ()) {
			askLeafToInsert(newKey);
		} else {
			if (!isFull ()) addKey (newKey);
			else {
				split(newKey);
			}
		}

		//Debug.Log (this);
	}

	public void propagateKey(int newKey, Page left, Page right) {
		if (!isFull ()) {
			int pos= findInsertPosition(newKey);

			addKey(newKey);
			leaves[pos]= left;
			left.enableLine(pos);
			left.transform.position= left.getPagePos();
			leaves[pos+1]= right;
			right.enableLine(pos+1);
			right.transform.position= right.getPagePos();

		}
		else {
			split(newKey);
		}
	}

	public void split(int newKey) {



		addKey (newKey);
		int median = keys[(keys.Count / 2)];
		keys.Remove (median);

		List<int> leftKeys 		= new List<int>();
		List<Page> leftLeaves 	= new List<Page>();
		List<int> rightKeys	 	= new List<int>();
		List<Page> rightLeaves 	= new List<Page>();

		for (int i=0; i< keys.Count; i++) { 
			if (keys[i] < median) {
				leftKeys.Add(keys[i]);
				leftLeaves.Add(leaves[i]);
			} else {
				rightKeys.Add(keys[i]);
				rightLeaves.Add(leaves[i]);
			}
		}
		rightLeaves.Add(leaves[leaves.Count - 1]);


		if (isRoot ()) {
			parent= createPage (this.transform.position);
			parent.setPage (null, level);
			parent.beRoot();
		}

		findInsertPosition(newKey);

		Page left = createPage (this.transform.position);
		left.setPage(parent,level, leftKeys, leftLeaves);

		Page right= createPage (this.transform.position);
		right.setPage(parent, level, rightKeys, rightLeaves);

		parent.propagateKey (median, left, right);
		
		Destroy (this.gameObject);
	}

	private Vector3 getPageLinePos(Vector3 pos) {
		Vector3 newPos = pos;
		newPos.x += (attachedToParentKeyPos * 1.0f) -2.0f;
		newPos.y -= 0.5f;
		return newPos;
	}

	private Vector3 getPagePos() {
		int newPosX= (attachedToParentKeyPos * 5) - 10;
		return new Vector3 (this.transform.position.x + newPosX, this.transform.position.y - 2, this.transform.position.z);
	}

//	private Vector3 getPageKeyLinePos(Vector3 pos) {
//		Vector3 newPos = pos;
//		newPos.x -= 1.0f;
//		newPos.y -= 0.5f;
//		return newPos;
//	}

//	private void addKey(int newKey) {
//		for (int key in keys) {
//			if (newKey < key)
//
//		}
//	}

	private void addKey(int newKey) {
		int pos= findInsertPosition(newKey);
		keys.Insert(pos, newKey);

		int counter= 0;
		foreach(Transform child in transform) {
			if (keys.Count > counter)
				child.GetChild (0).GetComponent<TextMesh> ().text = keys[counter].ToString();
			counter ++;
		}
//		Debug.Log("PageKey" + (pos+1 + "/Number"));
//		Transform number = children["PageKey" + (pos+1)];
//		foreach(Transform t in number) {.
//			t.GetComponent<TextMesh> ().text = newKey.ToString();
//		}
	}

	private int findInsertPosition(int newKey) {
		for (int i=0; i< keys.Count; i++) { 
			if (newKey < keys[i]) return i;
		}
		return keys.Count;
	}

	private void askLeafToInsert(int newKey) {
		bool flag = false;

		for(int i=0; i< keys.Count; i++) {
			if (newKey < keys[i]) {
				createLeaf(i, newKey);
				flag= true;
			}
		}
		// Ask last leaf
		if (!flag) {
			int last = keys.Count;
			createLeaf (last, newKey);
		}
	}

	private void createLeaf(int i, int newKey) {
		if (leaves[i] == null) {
			Page newPage= createPage (Vector3.zero);
			newPage.setPage(this, level);
			leaves[i]= newPage;
		}
		leaves[i].insertKey (newKey);
	}

	private bool isRoot() {
		return (parent == null);
	}

	public void beRoot() {
		root = this;
	}

	public Page getRoot() {
		return root;
	}

	// TODO: check if null?
	private bool isFull() {
		return (keys.Count == getMaxKeys());
	}

	private bool hasLeaves() {
		foreach (Page leaf in leaves) {
			if (leaf != null) return true;
		}
		return false;
	}


	public int getMaxKeys() {
		return (2 * level);
	} 

	public int getMaxLeaves() {
		return (2 * level + 1);
	}

	public override string ToString() {
		string zeKeyz = keysToString ();
		string zeLeavez = leavesToString ();
		return "Page: \n" + zeKeyz + "\n" + zeLeavez;
	}

	public string keysToString() {
		string result = "Keys[";
		foreach(int key in keys) {
			result += key;
			result += ", ";
		}
		result += "]";
		return result;
	}

	public string leavesToString() {
		string result = "Leaves[";
		foreach(Page leaf in leaves) {
			if (leaf == null) result += "null";
			else result += leaf.ToString();
			result += ", ";
		}
		result += "]";
		return result;

	}

	public Page createPage(Vector3 position) {
		GameObject pageObj= Instantiate (prefab, position ,Quaternion.identity) as GameObject;

		pageObj.transform.parent = transform.parent;
		return pageObj.GetComponent<Page> ();
	}

} 