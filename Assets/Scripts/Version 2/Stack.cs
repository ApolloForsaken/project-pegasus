using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Stack : MonoBehaviour {

	private Stack<Transform> stack;
	public Transform prefab;
	public float stackDepth;
	public float stackGap;
	public Transform deleteMe;

	public void Start() {
		stack= new Stack<Transform>();
	}

	public void push(int number) {
		
		Transform createdSun= (Transform) Instantiate (prefab, getPushPosition() , Quaternion.LookRotation(Vector3.forward));
		Planet planet = createdSun.GetComponent<Planet> ();
		planet.initialize(transform, number);
		createdSun.parent = transform;

		if (stack.Count != 0) {
			planet.enableLine(stack.Peek ());
		}

		animatePush (createdSun);
		stack.Push(createdSun);
		GameManagerScript.console.consoleWrite("Push: "+ number +"\n");
	}

	public void pop() {

		if (stack.Count == 0) {
			GameManagerScript.console.consoleWrite ("Stack is empty.");
		} else {
			Transform planet = stack.Pop ();
			planet.GetComponent<Planet>().disableLine();
			deleteMe= planet;
	
			GameManagerScript.console.consoleWrite ("Pop: " + planet.GetComponent<Planet>().number + "\n");
			animatePop(planet);
		}
	}

	public Vector3 getPushPosition() {
		Vector3 newPos = transform.position;
		newPos.x -= 20;
		newPos.y += stackDepth;
		return newPos;
	}

	public Vector3 getMiddlePosition() {
		Vector3 newPos = transform.position;
		newPos.y += stackDepth - 4;
		return newPos;
	}

	public Vector3 getPopPosition() {
		Vector3 newPos = transform.position;
		newPos.x += 40;
		newPos.y += stackDepth;
		return newPos;
	}

	public void animatePush(Transform planet) {
		disableInput ();
		Planet planetScript= planet.GetComponent<Planet>();


		Vector3 newPos;
		if (stack.Count == 0) {
			newPos = transform.position;
			newPos.y -= stackDepth;
		}
		else {
			newPos = stack.Peek ().position;
			newPos.y += stackGap;
		}

		Vector3 middlePos = newPos;
		middlePos.y += stackDepth;

		planetScript.moveLinear2 (getMiddlePosition(), newPos, afterAnimatePush);

	}

	public void animatePop(Transform planet) {
		disableInput ();
		Planet planetScript= planet.GetComponent<Planet>();
		planetScript.moveLinear2 (getMiddlePosition(), getPopPosition(), afterAnimationPop);
	}

	public void afterAnimatePush() {
		enableInput ();
	}

	public void afterAnimationPop() {
		enableInput ();
		Destroy (deleteMe.gameObject);
	}
	
	
	public void enableInput() {
		foreach (Button child in GameManagerScript.stackPanel.GetComponentsInChildren<Button>()) {
			child.interactable= true;
		}	
		foreach (InputField child in GameManagerScript.stackPanel.GetComponentsInChildren<InputField>()) {
			child.interactable= true;
		}
	}
	
	public void disableInput() {
		foreach (Button child in GameManagerScript.stackPanel.GetComponentsInChildren<Button>()) {
			child.interactable= false;
		}	
		foreach (InputField child in GameManagerScript.stackPanel.GetComponentsInChildren<InputField>()) {
			child.interactable= false;
		}
	}

}