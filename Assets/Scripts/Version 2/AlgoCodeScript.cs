﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AlgoCodeScript : MonoBehaviour {

	private GameObject[] lines;
	public Color defaultLineBackgroundColor;
	
	protected void Start () {
		lines= GameObject.FindGameObjectsWithTag ("AlgoLine");
		defaultLineBackgroundColor = lines [0].GetComponent<Image> ().color;
	}

	public void replaceLine(int number, string replaceWith) {

		setLine (number, replaceWith);
		highlightLine(number);
	}

	public void setLine(int number, string replaceWith) {
		Transform panel= transform.FindChild("AlgoLine" + number);

		Text line= panel.FindChild("Line").GetComponent<Text>();

		// Replace Text.
		line.text= replaceWith;
	}

	public void highlightLine(int number) {
		Transform panel= transform.FindChild("AlgoLine" + number);

		deHighlightLines ();
		panel.GetComponent<Image>().color= Color.green;
	}

	public void deHighlightLines() {
		foreach (GameObject line in lines) {
				line.GetComponent<Image> ().color = defaultLineBackgroundColor;
		}
	}
}
