using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Queue : MonoBehaviour {
	
	private Queue<Transform> queue;
	public Transform prefab;
	public float queueStart;
	public float queueGap;
	private Transform deleteMe;
	private Transform previous;
	
	public void Start() {
		queue= new Queue<Transform>();
	}
	
	public void enqueue(int number) {

		Transform createdSun= (Transform) Instantiate (prefab, getEnqueuePosition() , Quaternion.LookRotation(Vector3.forward));
		Planet planet = createdSun.GetComponent<Planet> ();
		planet.initialize(transform, number);

		if (queue.Count != 0) {
			planet.enableLine(previous);
		}

		previous = createdSun;

		animateEnqueue (createdSun);
		queue.Enqueue(createdSun);
		GameManagerScript.console.consoleWrite("Enqueue: "+ number +"\n");
	}
	
	public void dequeue() {

		if (queue.Count == 0) {
			GameManagerScript.console.consoleWrite ("Queue is empty.");
		} else {
			Transform planet = queue.Dequeue ();
			queue.Peek ().GetComponent<Planet>().disableLine();
			deleteMe= planet;

			GameManagerScript.console.consoleWrite ("Dequeue: " + planet.GetComponent<Planet>().number + "\n");
			animateDequeue(planet);
		}
	}
	
	public Vector3 getEnqueuePosition() {
		Vector3 newPos = transform.position;
		newPos.x -= 20;
		return newPos;
	}
	
	public Vector3 getDequeuePosition() {
		Vector3 newPos = transform.position;
		newPos.x += 20;
		return newPos;
	}
	
	public void animateEnqueue(Transform planet) {
		disableInput ();
		Planet planetScript= planet.GetComponent<Planet>();
		
		
		Vector3 newPos;
		newPos = transform.position;
		newPos.x -= queueStart;
		
		planetScript.moveLinear (newPos, afterAnimateEnqueue);

		Planet[] planets= GetComponentsInChildren<Planet>();
		foreach(Planet planetz in planets) {
			if(planetz.Equals(planetScript)) {
				Debug.Log ("found me! ");
				continue;
			}
			Vector3 newPoz= planetz.transform.position;
			newPoz.x += queueGap;
			planetz.moveLinear (newPoz, afterAnimateEnqueue, false);
			Debug.Log ("Pos: " + newPoz.x);
		}

	}
	
	public void animateDequeue(Transform planet) {
		disableInput ();
		Planet planetScript= planet.GetComponent<Planet>();
		planetScript.moveLinear (getDequeuePosition(), afterAnimateDequeue);
	}

	public void afterAnimateEnqueue() {
		enableInput();
	}

	public void afterAnimateDequeue() {
		enableInput();
		Destroy (deleteMe.gameObject);
	}

	public void enableInput() {
		foreach (Button child in GameManagerScript.queuePanel.GetComponentsInChildren<Button>()) {
			child.interactable= true;
		}	
		foreach (InputField child in GameManagerScript.queuePanel.GetComponentsInChildren<InputField>()) {
			child.interactable= true;
		}
	}
	
	public void disableInput() {
		foreach (Button child in GameManagerScript.queuePanel.GetComponentsInChildren<Button>()) {
			child.interactable= false;
		}	
		foreach (InputField child in GameManagerScript.queuePanel.GetComponentsInChildren<InputField>()) {
			child.interactable= false;
		}
	}

}