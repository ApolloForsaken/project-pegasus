﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class BubbleSortScript : Sun {
	
	public override void run() {
		generateBubbleSortList (getPlanetList());
	}

	public override string getPseudoCode () {
		return "procedure bubbleSort( A : list of sortable items )\n" +
			"	n = length(A)\n" +
			"	repeat\n" +
			"		newn = 0\n" +
			"		for i = 1 to n-1 inclusive do\n" +
			"			if A[i-1] > A[i] then\n" +
			"				swap(A[i-1], A[i])\n" +
			"				newn = i\n" +
			"			end if\n" +
			"		end for\n" +
			"		n = newn\n" +
			"	until n = 0\n" +
			"end procedure\n";
	}

	private void generateBubbleSortList( List<Planet> currentList) {
 		
		List<Planet> currentPlanetList = new List<Planet> ();
	
		currentPlanetList.AddRange (currentList);
		
		bool swapped=false;

		getStepList ().add (new BubbleScanListStep (currentList));

		for (int i=0; i< currentPlanetList.Count-1; i++) {

			bool currentSwapped= currentPlanetList [i].getNumber() > currentPlanetList [i + 1].getNumber();

			getStepList ().add (new BubbleCompareStep (i, i+1, currentSwapped));

			if (currentSwapped){
				BubbleCompareStep.swapPlanets (ref currentPlanetList, i, i+1);
				swapped = true;
			}
		}

		if (swapped) {
			Debug.Log ("Looper!");
			currentPlanetList.RemoveAt (currentPlanetList.Count - 1);
			generateBubbleSortList (currentPlanetList);
		} else {
			Debug.Log ("Ta Kataferame");
		}

	}
}