﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class QSCompareStep : Step<Planet> {
	
	private int planet;
	private int pivot;
	bool swap;


	public QSCompareStep(int planet, int pivot, bool swap) {
		this.planet = planet;
		this.pivot = pivot;
		this.swap = swap;
	}

	public override void run(ref List<Planet> planetList) {
		Debug.Log ("Swap: "+ planetList[planet].ToString() + " with "+ planetList[pivot].ToString() + " " + swap);

		if (swap) {
			animate (planetList);
			algoCode.replaceLine(3, "Swap: " 		+ planetList[planet].number + " > " + planetList[pivot].number );
			swapPlanets (ref planetList, planet, pivot);
		} else {
			algoCode.replaceLine(4, "Don't Swap: " + planetList[planet].number + " <= " + planetList[pivot].number );
		}

		DebugScript.debugPlanetList (planetList);
	}

	public override void reverse(ref List<Planet> planetList) {
		if (swap) {
			reverseAnimate (planetList);
			reverseSwapPlanets (ref planetList, planet, pivot);
		}
	}

	public static void swapPlanets(ref List<Planet> planetList, int planet, int pivot) {

		Planet planetPlanet = planetList [planet];
		Planet pivotPlanet = planetList [pivot];
		Planet pivotPrevPlanet = planetList [pivot - 1];

		if (Math.Abs (pivot - planet) == 1) {
		
			planetList [planet] = pivotPlanet;
			planetList [pivot] = planetPlanet;

		} else {
				
			planetList [pivot-1] = pivotPlanet;
			planetList [planet] = pivotPrevPlanet;
			planetList [pivot] =  planetPlanet;

		}
	}

	// TODO: move to STEP
	private void animate(List<Planet> planetList) {

		Planet planetScript= planetList[planet].GetComponent<Planet>();
		Planet pivotScript= planetList [pivot].GetComponent<Planet>();
		Planet pivotPreviousScript=planetList [pivot - 1].GetComponent<Planet>();

		if (Math.Abs (pivot - planet) == 1) {

			pivotScript.move (planetList [planet].transform.position, Vector3.down);
			planetScript.move(planetList [pivot].transform.position, Vector3.up);
			
			
		} else {
			
			planetScript.move(planetList [pivot].transform.position, Vector3.up);
			pivotScript.move(planetList [pivot - 1].transform.position, Vector3.down);
			pivotPreviousScript.move(planetList[planet].transform.position, Vector3.down);
			
		}
	}

	public static void reverseSwapPlanets(ref List<Planet> planetList, int planet, int pivot) {
		
		Planet planetPlanet = planetList [planet];
		Planet pivotPlanet = planetList [pivot];
		Planet pivotPrevPlanet = planetList [pivot - 1];
		
		if (Math.Abs (pivot - planet) == 1) {
			
			planetList [planet] = pivotPlanet;
			planetList [pivot] = planetPlanet;
			
		} else {
			
			planetList [pivot-1] = planetPlanet;
			planetList [planet] = pivotPlanet;
			planetList [pivot] =  pivotPrevPlanet;
			
		}
	}
	
	// TODO: move to STEP
	private void reverseAnimate(List<Planet> planetList) {
		
		Planet planetScript= planetList[planet].GetComponent<Planet>();
		Planet pivotScript= planetList [pivot].GetComponent<Planet>();
		Planet pivotPreviousScript=planetList [pivot - 1].GetComponent<Planet>();
		
		if (Math.Abs (pivot - planet) == 1) {
			
			pivotScript.move (planetList [planet].transform.position, Vector3.down);
			planetScript.move(planetList [pivot].transform.position, Vector3.up);
			
			
		} else {
			
			planetScript.move(planetList [pivot-1].transform.position, Vector3.down);
			pivotScript.move(planetList [planet].transform.position, Vector3.up);
			pivotPreviousScript.move(planetList[pivot].transform.position, Vector3.up);
			
		}
	}
}
