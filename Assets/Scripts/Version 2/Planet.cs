﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Planet : MonoBehaviour {

	public delegate void AfterAnimation();

	private Transform planetObject;
	private Transform planetCanvas;
	private Transform planetNumber;
	//private float rotationSpeed;
	private float size;
	private static Color defaultColor;
	private float speed;
	public int number;


	private Transform attachedTo;
	private LineRenderer line;

	private Transform mainCamera;

	public float minimumScale;
	public float maximumScale;

	public void Awake() {
		speed = 30 * (1 / transform.position.x + 2);

		planetObject= transform.FindChild("PlanetObject");
		planetCanvas= transform.FindChild("Canvas");
		planetNumber = planetCanvas.FindChild ("Number");
		line= GetComponent<LineRenderer> ();
		attachedTo = transform;
		line.enabled = false;

		defaultColor = planetObject.light.color;
		mainCamera = Camera.main.transform;
	}


	/**
	 *  Initialize Planet Prefab, (scale color etc.)
	 */
	public void initialize(Transform parent, int _number, Material material, bool scaleMe) {
		number = _number;

		if (scaleMe) {
			float scale = (number * maximumScale / 100) + minimumScale;
			transform.localScale = new Vector3 (scale, scale, scale);
		}

		planetObject.renderer.material = material;
		// TODO: call planet script.	
		transform.parent= parent;
		
		planetNumber.GetComponent<Text> ().text = number.ToString();
	}

	public void initialize(Transform parent, int _number) {

		Material material= GameManagerScript.materials[Random.Range(0, GameManagerScript.materials.Count - 1)];

		initialize (parent, _number, material, false);
	}

	public void Update() {

		line.SetPosition (0, transform.position);
		line.SetPosition (1, attachedTo.position);

		// Look At Camera
		//planetCanvas.LookAt (mainCamera);

		// Rotate Around Sun TODO: remove hardcoded shit 
//		transform.RotateAround(new Vector3(-2,0,0), Vector3.up, speed * Time.deltaTime);

		// Rotate Around Self
		planetObject.RotateAround(planetObject.position, Vector3.left, 5 * Time.deltaTime);
		planetObject.RotateAround(planetObject.position, Vector3.down, 20 * Time.deltaTime);
	}

	public void highlight(bool yes, Color color, float range) {
		if (yes) {
			planetObject.light.color= color;
			planetObject.light.renderer.light.range= this.transform.localScale.x * range;
		} else {
			planetObject.light.renderer.light.range= 0;
		}
	}

	public void highlight(bool yes, Color color) {
		highlight (yes, color, 5f);
	}

	public void highlight(bool yes) {
		highlight (yes, defaultColor);
	}

	public void move(Vector3 target, Vector3 direction) {

		//Debug.Log ("I move: " + this.name + " " + target);
		float speed = 2.0f;

		//StartCoroutine (moveCoRoutine (startTime, journeyLength, target));
		StartCoroutine (moveAroundCoRoutine (speed, this.transform.position, target, direction));
		planetObject.audio.Play ();
	}


	public void moveLinear(Vector3 target, AfterAnimation callAfter) {
		moveLinear (target, callAfter, true);
		
	}

	// FIXME fix this duplicate shit.
	// Fixed. now the shit is triple.
	public void moveLinear(Vector3 target, AfterAnimation callAfter, bool playAudio) {
		float seconds = 1f;
//		float speed = 10.0f;
		StartCoroutine(coRoutineMoveLinear(seconds, this.transform.position, target, callAfter));
		if (playAudio) {
				planetObject.audio.Play ();
		}
	}

	public void moveLinear2(Vector3 target1, Vector3 target2, AfterAnimation callAfter) {
		float speed = 10.0f;
		StartCoroutine(coRoutineMoveLinear2(speed, this.transform.position, target1, target2, callAfter));
		planetObject.audio.Play ();
	}

	// Slerp (move around center of start/target 
	public IEnumerator moveAroundCoRoutine(float speed, Vector3 start, Vector3 target, Vector3 direction) {
		
		float startTime = Time.time;
		Vector3 center, riseRelCenter, setRelCenter;
		float fracJourney = 0;

		while (fracJourney <= 1){

			center = (start + target) * 0.5F;
			center -= direction;
			riseRelCenter = start - center;
			setRelCenter = target - center;
			fracJourney = (Time.time - startTime) / speed;
			transform.position = Vector3.Slerp(riseRelCenter, setRelCenter, fracJourney);
			transform.position += center;

			yield return null;
		}
	}

	public IEnumerator coRoutineMoveLinear(float seconds, Vector3 start, Vector3 target, AfterAnimation callAfter) {

		float startTime = Time.time;
		float journeyLength = Vector3.Distance(start, target);
		float fracJourney = 0;
		float speed = journeyLength * seconds;

		while (fracJourney <= 1) {

			// Distance moved = time * speed.
			var distCovered = (Time.time - startTime) * speed;

			// Fraction of journey completed = current distance divided by total distance.
			fracJourney = distCovered / journeyLength;

			// Set our position as a fraction of the distance between the markers.
			transform.position = Vector3.Lerp (start, target, fracJourney);

			yield return null;
		}

		callAfter();
	}

	public IEnumerator coRoutineMoveLinear2(float speed, Vector3 start, Vector3 target1, Vector3 target2, AfterAnimation callAfter) {
		
		float startTime = Time.time;
		float journeyLength = Vector3.Distance(start, target1);
		float fracJourney = 0;
		
		while (fracJourney <= 1) {
			
			// Distance moved = time * speed.
			var distCovered = (Time.time - startTime) * speed;
			
			// Fraction of journey completed = current distance divided by total distance.
			fracJourney = distCovered / journeyLength;
			
			// Set our position as a fraction of the distance between the markers.
			transform.position = Vector3.Lerp (start, target1, fracJourney);
			
			yield return null;
		}

		startTime = Time.time;
		journeyLength = Vector3.Distance(target1, target2);
		fracJourney = 0;

		while (fracJourney <= 1) {
			
			// Distance moved = time * speed.
			var distCovered = (Time.time - startTime) * speed;
			
			// Fraction of journey completed = current distance divided by total distance.
			fracJourney = distCovered / journeyLength;
			
			// Set our position as a fraction of the distance between the markers.
			transform.position = Vector3.Lerp (target1, target2, fracJourney);
			
			yield return null;
		}

		callAfter ();
	}



	public float getSize() {
		return size;
	}

	public int getNumber() {
		return number;
	}

	// TODO: Does not work atm.
	public override string ToString() {
		return "Planet: " + name + " | " + size;
	}

	public void enableLine(Transform attachTo) {
		line.enabled = true;
		attachedTo = attachTo;
	}

	public void disableLine() {
		line.enabled = false;
		attachedTo = transform;
	}
}
