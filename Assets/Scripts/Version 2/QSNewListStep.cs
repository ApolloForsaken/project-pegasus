﻿using UnityEngine;
using System.Collections.Generic;

public class QSNewListStep : Step<Planet> {
	
	private List<Planet> newList;

	public QSNewListStep(List<Planet> newList) {
		this.newList = newList;
	}

	public override void run(ref List<Planet> lst) {
		
		string planetList = "";

		foreach (Planet q in lst) {
			q.highlight(false);
		}

		foreach(Planet go in newList) {
			go.highlight(true);
			planetList+= go.number;
			planetList+= " ,";
		}

		algoCode.replaceLine(1, "New List: " + planetList);
	}

	public override void reverse(ref List<Planet> planetList) {
		foreach (Planet q in planetList) {
			q.highlight(false);
		}
	}
}
