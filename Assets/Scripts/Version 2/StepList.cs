﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StepList<T> where T : MonoBehaviour {

	private List<Step<T>> stepList;
	private List<T> planetList;
	private int currentStep;
	
	public void Start () {
		currentStep = 0;
	}

	public StepList(List<T> planetList) {
		this.planetList = planetList;
		stepList = new List<Step<T>> ();
	}	

	public void add(Step<T> step) {
		stepList.Add (step);
	}

	public void next() {
		if (stepList.Count > currentStep) {
			stepList [currentStep++].run (ref planetList);
		}
	}

	public bool hasNext() {
		return stepList.Count > currentStep;
	}

	public void previous() {
		if (currentStep >= 1) {
			stepList [--currentStep].reverse (ref planetList);
		}
	}

}
