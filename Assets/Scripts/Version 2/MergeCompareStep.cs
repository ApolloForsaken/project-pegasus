﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class MergeCompareStep : Step<Planet> {
	
	private List<Planet> leftList= new List<Planet> ();
	private List<Planet> rightList= new List<Planet> ();
	private Planet planet;
	private int nextPosition;
	private bool firstHalf;
	private Vector3 targetPosition;
	private bool isLeft;

	// Reverse
	private Planet previousPlanet;
	private Vector3 previousPosition;
	
	public MergeCompareStep(List<Planet> _leftList, List<Planet> _rightList, int _nextPosition, Vector3 targetPosition) {
		this.leftList.AddRange(_leftList);
		this.rightList.AddRange(_rightList);
		this.nextPosition = _nextPosition;
		this.targetPosition = targetPosition;
		this.planet=  nonStaticmergePlanets (ref leftList, ref rightList);
	}
	
	public override void run(ref List<Planet> planetList) {

		// Reverse
		this.previousPlanet = planetList [nextPosition];
		this.previousPosition = planet.transform.position;

		planetList [nextPosition]= planet;
		animate (planetList);

		if (isLeft) {
			algoCode.replaceLine (3, "Copy Left Element: " + planet.number);
		} else {
			algoCode.replaceLine (4, "Copy Right Element: " + planet.number);
		}
	}

	public override void reverse(ref List<Planet> planetList) {
		planetList [nextPosition]= previousPlanet;
		planet.move(previousPosition, Vector3.down);
	}

	// TODO: FIX THIS SHIT (with static as well as this)
	public Planet nonStaticmergePlanets(ref List<Planet> leftList, ref List<Planet> rightList) {
		
		Planet planet;
		
		if (leftList.Count != 0 && rightList.Count != 0) {
			if( leftList[0].transform.localScale.x < rightList[0].transform.localScale.x ) {
				planet= leftList[0];
				leftList.RemoveAt(0);
				isLeft= true;
				
			} else {
				planet= rightList[0];
				rightList.RemoveAt(0);
				isLeft= false;
			}
			
		} else if (leftList.Count != 0) {
			planet= leftList[0];
			leftList.RemoveAt(0);
			isLeft= true;
		} else {
			planet= rightList[0];
			rightList.RemoveAt(0);
			isLeft= false;
		}
		
		return planet;
	}
	
	public static Planet mergePlanets(ref List<Planet> leftList, ref List<Planet> rightList) {

		Planet planet;

		if (leftList.Count != 0 && rightList.Count != 0) {
			if( leftList[0].transform.localScale.x < rightList[0].transform.localScale.x ) {
				planet= leftList[0];
				leftList.RemoveAt(0);

			} else {
				planet= rightList[0];
				rightList.RemoveAt(0);
			}

		} else if (leftList.Count != 0) {
			planet= leftList[0];
			leftList.RemoveAt(0);
		} else {
			planet= rightList[0];
			rightList.RemoveAt(0);
		}

		return planet;
	}
	
	// TODO: move to STEP

	private void animate(List<Planet> planetList) {
		planet.move(targetPosition, Vector3.up);
	}
}
