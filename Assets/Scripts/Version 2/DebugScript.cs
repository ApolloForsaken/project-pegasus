using UnityEngine;
using System.Collections.Generic;

public class DebugScript {
	
	public static void debugPlanet(Planet planet) {
		Debug.Log ("Planet: " + planet.name + ", size: " + planet.getSize());
	}

	public static void debugPlanetList(List<Planet> planetList) {
	
		string debugString = "Current PlanetList: ";
		foreach (Planet planet in planetList) {
			debugString += "QSP-" + planet.name.Substring(planet.name.Length - 1);
			debugString += " / ";
		}
		Debug.Log (debugString);
		
	}
}
