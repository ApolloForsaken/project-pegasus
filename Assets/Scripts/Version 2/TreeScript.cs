﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TreeScript : MonoBehaviour {
	
	public BTreeLeafScript root;
	public GameObject prefab;
	public float startPosY;

	public GameObject pageKey;
	public GameObject pageKeyPrefab;
	public float pageKeyPosY;
	public float waitSeconds;
	private List<BTreeLeafScript> removeLeavesList;

	void Start () {
		removeLeavesList = new List<BTreeLeafScript> ();
	}

	public IEnumerator createTree(int key, int level) {

		Vector3 startPos = transform.position;
		startPos.y += startPosY;

		//root= Instantiate (prefab, startPos, Quaternion.identity) as BTreeLeafScript;
		GameObject rootObj= Instantiate (prefab, startPos, Quaternion.identity) as GameObject;
		root= rootObj.GetComponent<BTreeLeafScript> ();

		root.create (level);
		root.line.enabled = false;
		yield return StartCoroutine(root.insertKey (key));
	}

	public IEnumerator insert(int insertMe) {
	
		disableInput ();
		if (root == null) {
			yield return StartCoroutine(createTree (insertMe, 3));
		} else {
			yield return StartCoroutine(root.insertKey (insertMe));
		}
		enableInput ();
		removeLeaves ();
	}

	
	public IEnumerator delete(int deleteMe) {

		disableInput ();
		if (root != null) {
			yield return StartCoroutine(root.deleteKey(deleteMe));
		}
		enableInput ();
		removeLeaves (); 

	}

	public void enableInput() {
		foreach (Transform child in GameManagerScript.treePanel.transform) {
			child.GetComponent<InputField>().interactable= true;
		}
	}

	public void disableInput() {
		foreach (Transform child in GameManagerScript.treePanel.transform) {
			child.GetComponent<InputField>().interactable= false;
		}
	}


	public IEnumerator changeKey(int key) {
		pageKey.transform.GetChild(0).GetComponent<TextMesh> ().text = key.ToString ();
		
		yield return StartCoroutine (highlightKey (Color.white));
	}

	public IEnumerator addKey(Vector3 pos, int key, Color color) {
		pos.y += pageKeyPosY;
		pageKey= Instantiate (pageKeyPrefab, pos, Quaternion.identity) as GameObject;
		pageKey.transform.parent = transform;
		pageKey.transform.GetChild(0).GetComponent<TextMesh> ().text = key.ToString ();
		pageKey.renderer.material.color = color;

		yield return StartCoroutine (highlightKey (Color.white));
	}



	public void deleteKey() {
		if (pageKey != null)
			Destroy (pageKey);
	}

	public IEnumerator highlightKey(Color color) {
		pageKey.gameObject.light.color= color;
		pageKey.gameObject.light.renderer.light.range= 3;
		
		yield return (new WaitForSeconds (waitSeconds));
		
		pageKey.gameObject.light.renderer.light.range= 0;
	}

	public IEnumerator moveKey(Vector3 newPos, int newKey, Color color) {

		if (pageKey == null) {
			yield return StartCoroutine(addKey(newPos, newKey, color));
		}

		newPos.y += pageKeyPosY;
		yield return StartCoroutine(coRoutineMoveLinear(pageKey, 10f, pageKey.transform.position, newPos));
	}

	public IEnumerator coRoutineMoveLinear(GameObject go, float speed, Vector3 start, Vector3 target) {

		if (!start.Equals (target)) {

			float startTime = Time.time;
			float journeyLength = Vector3.Distance (start, target);
			float fracJourney = 0;

			while (fracJourney <= 1) {

				// Distance moved = time * speed.
				var distCovered = (Time.time - startTime) * speed;

				// Fraction of journey completed = current distance divided by total distance.
				fracJourney = distCovered / journeyLength;

				// Set our position as a fraction of the distance between the markers.
				go.transform.position = Vector3.Lerp (start, target, fracJourney);

				yield return null;
			}
		}

	}

	public void addRemoveMeLeaf(BTreeLeafScript removeMeLeaf) {
		removeLeavesList.Add (removeMeLeaf);

		foreach( Transform child in removeMeLeaf.transform ){
			Destroy (child.gameObject);
		}
		removeMeLeaf.line.enabled = false;
	}

	private void removeLeaves() {
		foreach( BTreeLeafScript leaf in removeLeavesList) {
			Destroy (leaf.gameObject);
		}

		removeLeavesList.Clear ();
	}

}
