﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConsoleScript : MonoBehaviour {

	public float blinkSpeed;
	public float writeSpeed;
	private static string DASH= "_";
	private static char[] TRIM_DASH= {'_'};
	private Text textArea;
	private int defaultSize = 12;

	// Use this for initialization
	void Start () {
		textArea = this.GetComponentInChildren<Text>();
	}

	public void consoleWrite(string toWrite) {
		StartCoroutine (write(toWrite));
	}

	private IEnumerator write(string toWrite) {
		StopCoroutine ("blink");
		textArea.text= textArea.text.TrimEnd(TRIM_DASH);
		foreach (char c in toWrite) {
			textArea.text= textArea.text + c;
			yield return new WaitForSeconds (writeSpeed);
		}
		StartCoroutine ("blink");
	}

	private IEnumerator blink() {
		textArea.text= textArea.text.TrimEnd(TRIM_DASH);
		while (true) {
			textArea.text= textArea.text.Insert (textArea.text.Length, DASH);
			yield return new WaitForSeconds (blinkSpeed);
			textArea.text= textArea.text.TrimEnd(TRIM_DASH);
			yield return new WaitForSeconds (blinkSpeed);
		}
	}

	public void reset() {
		textArea.fontSize= defaultSize;
		textArea.text = "";
	}

	public void copy(string text, int fontSize) {
		reset ();
		textArea.text = text;
		textArea.fontSize = fontSize;
	}
}
