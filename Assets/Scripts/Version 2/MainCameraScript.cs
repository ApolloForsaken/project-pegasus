﻿using UnityEngine;
using System.Collections;

public class MainCameraScript : MonoBehaviour {

	private Vector3 newPos;
	private Vector3 newRot;
	public float cameraSpeed;
	public float cameraAcceleration;
	private float defaultCameraSpeed;
	public float zoomDistance;

	// Use this for initialization
	void Start () {
		defaultCameraSpeed= cameraSpeed;
		newPos = new Vector3 (5, 5, -5);
		newRot = new Vector3 (45, 0, 0);
	}

	void Update() {
		if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
			transform.position += transform.forward * zoomDistance;
		} else if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
			transform.position -= transform.forward * zoomDistance;
		}
	}

	// ### SORT
	public IEnumerator focusSun(Transform activeSun) {
		Vector3 focusPos = activeSun.position;
		newPos.x = focusPos.x + 5;
		newPos.y= focusPos.y + 5;
		newPos.z= focusPos.z - 5; 
		Quaternion newRot= Quaternion.LookRotation(Vector3.forward + Vector3.down);

		yield return StartCoroutine (moveCoRoutine (newPos, newRot));
	}

	// ### STACK / QUEUE / (TREE?)
	public IEnumerator focusHole(Transform hole) {
		Vector3 focusPos = hole.position;

		Quaternion newRot= Quaternion.LookRotation(Vector3.forward);
		focusPos.z= focusPos.z - 20; 

		yield return StartCoroutine (moveCoRoutine (focusPos, newRot));
	}

	// ### NODE LIST
	public IEnumerator focusGalaxy(Transform blackHole) {
		Vector3 focusPos = blackHole.position;
		Vector3 newPos= focusPos;
		newPos.y= focusPos.y + 20; 

		Quaternion newRot=  Quaternion.LookRotation(Vector3.down);
		
		yield return StartCoroutine (moveCoRoutine (newPos, newRot));
	}

	public void focusNode(Node node) {
		Vector3 focusPos = node.getPlanet().transform.position;
		newPos = focusPos;
		newPos.z= focusPos.z - 5; 
		Quaternion newRot= Quaternion.LookRotation(Vector3.forward);
		
		StartCoroutine (moveCoRoutine (newPos, newRot));
	}

	// Simple Lerp (linear move)
	public IEnumerator moveCoRoutine(Vector3 newPos, Quaternion newRot) {

		cameraSpeed= defaultCameraSpeed;

		Vector3 startPos= transform.position;
		Quaternion startRot= transform.rotation;
		float startTime = Time.time;

		float journeyLength = Vector3.Distance(startPos, newPos);
		float distCovered;
		float fracJourney = 0;
		while (fracJourney <= 1) {
			cameraSpeed+= cameraAcceleration;

			distCovered = (Time.time - startTime) * cameraSpeed;
			fracJourney = distCovered / journeyLength;
			transform.position = Vector3.Lerp (startPos, newPos, fracJourney);
			transform.rotation= Quaternion.Lerp(startRot, newRot, fracJourney);

			yield return null;
		}

		cameraSpeed= defaultCameraSpeed;

	}




}