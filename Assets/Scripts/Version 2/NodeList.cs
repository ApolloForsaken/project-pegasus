﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NodeList : MonoBehaviour {
//	LinkedListNode


	public static Vector3 DEFAULT_CAMERA_POSITION= new Vector3(0, 20, 0) ;
	public static Vector3 DEFAULT_CAMERA_ROTATION= Vector3.down;
	public static int PLANET_CAMERA_POSITION_Z= -2;
	public static Vector3 PLANET_CAMERA_ROTATION= Vector3.forward;

	private Dictionary<Vector3, Node> galaxyMap = new Dictionary<Vector3, Node>();

	public Transform prefab;
	private Node firstNode;
	private Node currentNode;

	private float speed= 5.0f;
	private float startTime;

	public float minX;
	public float maxX;
	public float minZ;
	public float maxZ;
	public float gap;
	private float highlightNumber= 2;

	public Color defaultLineStartColor;
	public Color defaultLineEndColor;

	public float waitSeconds;

	public void Start() {
		GameManagerScript.nodePanel.transform.FindChild ("StopButton").GetComponent<Button>().interactable= false;
	}

	public Node insert(int number) {
	
		Vector3 nextPosition;

		do {
			nextPosition= getRandomPosition ();
		} while (galaxyMap.ContainsKey(nextPosition));

		// Add Next
		Transform createdSun= (Transform) Instantiate (prefab, nextPosition , Quaternion.LookRotation(Vector3.down));
		Planet planet = createdSun.GetComponent<Planet> ();
		planet.initialize(transform, number);

		Node nextNode= new Node (createdSun);
		if (currentNode != null) {

			// IF CURRENT HAD ALREADY NEXT NODE
			if(currentNode.getNext () != Vector3.zero) {
				// SET NEW AS PREVIOUS NODE
				nextNode.setNextPosition(currentNode.getNext ());
				galaxyMap[currentNode.getNext ()].setPrevPosition(nextPosition);
				galaxyMap[currentNode.getNext ()].getPlanet().enableLine(planet.transform);
			}

			currentNode.setNextPosition(nextPosition);
			nextNode.setPrevPosition(currentNode.getPosition ());

		} 

		galaxyMap.Add (nextPosition, nextNode);

		if (firstNode == null) {
			firstNode = nextNode;
			Debug.Log ("found First");
		} else {
			planet.enableLine(currentNode.getPlanet().transform);
			currentNode.getPlanet ().highlight(false);
		}

		nextNode.getPlanet ().highlight (true, Color.green, highlightNumber);

		// Show Next
		currentNode= nextNode;

		GameManagerScript.console.consoleWrite ("Insert Node(Data: "+ number +", Next: "+ currentNode.getNext () +" )");


		return currentNode;
	}

	public Node delete() {
		if (currentNode != null) {
			
			Node prevNode= currentNode;
			GameManagerScript.console.consoleWrite("Delete Node(Data: " + prevNode.getPlanet().getNumber() + ")");

			prevNode.getPlanet ().highlight (true, Color.red, highlightNumber);
			prevNode.getPlanet ().disableLine();

			if (prevNode.getPrev () != null) {

				// SET PREVIOUS to next node
				if (prevNode.getNext () != Vector3.zero) {
					Debug.Log (prevNode.getNext ());
					Node nextNode= galaxyMap[prevNode.getNext ()];
					
					nextNode.setPrevPosition(prevNode.getPrev ());
					nextNode.getPlanet().enableLine(galaxyMap[prevNode.getPrev ()].getPlanet().transform);
				}

				// SET NEW CURRENT NODE
				// SET NEXT to previous node

				if (currentNode.getPrev () != Vector3.zero) {
					currentNode= galaxyMap[currentNode.getPrev ()];
					currentNode.getPlanet ().highlight (true, Color.white, highlightNumber);
					currentNode.setNextPosition(prevNode.getNext ());
				} else {
					firstNode= null;
				}

				GameManagerScript.console.consoleWrite("Current Node(Data:  "+ currentNode.getPlanet().getNumber() +", Next: "+ currentNode.getNext () +" )");
			} else {

				GameManagerScript.console.consoleWrite("List is Empty");
				firstNode= null;
			}

		}

		return currentNode;
	}

	// TODO: Fix this
	public Vector3 getRandomPosition() {
		return transform.position + new Vector3 (Random.Range(minX * gap, maxX * gap), 0, Random.Range(minZ * gap, maxZ * gap) );
	}

	public Node getFirst() {

		if (firstNode != null) {
			setAndHighlight (firstNode);
		}

		return firstNode;
	}

	public Node getCurrent() {
		return currentNode;
	}

	public Node getNext() {

		if (currentNode != null && currentNode.getNext () != Vector3.zero) {
			setAndHighlight (galaxyMap [currentNode.getNext ()]);
		}

		return currentNode;
	}

	public Node getPrevious() {

		if (currentNode != null && currentNode.getPrev () != Vector3.zero) {
			setAndHighlight (galaxyMap [currentNode.getPrev ()]);
		}
		
		return currentNode;
	}

	public void setAndHighlight(Node newNode) {
		currentNode.getPlanet ().highlight(false);
		currentNode = newNode;
		currentNode.getPlanet ().highlight (true, Color.white, highlightNumber);
	}
	
	public void run() {
		if (firstNode != null) {
			currentNode.getPlanet().highlight(false);
			currentNode = firstNode;
			StartCoroutine ("Run", waitSeconds);
			GameManagerScript.nodePanel.transform.FindChild ("StopButton").GetComponent<Button> ().interactable = true;
		}
	}
	
	public void stop() {
		enableInput ();
		GameManagerScript.nodePanel.transform.FindChild ("StopButton").GetComponent<Button>().interactable= false;
		if (currentNode != null) {
			currentNode.getPlanet ().highlight (true, Color.white, highlightNumber);
		}

		StopCoroutine("Run");
	}

	public IEnumerator Run(int seconds) {

		disableInput ();

		setAndHighlight (galaxyMap [currentNode.getPosition()]);
		yield return new WaitForSeconds (seconds);

		while (currentNode.getNext() != Vector3.zero) {
			currentNode.getPlanet ().highlight(false);
			currentNode = galaxyMap [currentNode.getNext ()];
			yield return StartCoroutine(highlightLine(seconds));
			currentNode.getPlanet ().highlight (true, Color.white, highlightNumber);
			yield return new WaitForSeconds (seconds);
		}

		enableInput ();
	}

	public IEnumerator highlightLine(int seconds) {
		
		LineRenderer line= currentNode.getPlanet ().GetComponent<LineRenderer>(); 
		line.SetColors (Color.white, Color.white);
//		line.SetWidth(0.2f, 0.2f);
		
		yield return new WaitForSeconds (seconds);
		
		line.SetColors (defaultLineStartColor, defaultLineEndColor);
		line.SetWidth(0.1f, 0.1f);
	}

	public List<int> getListNumbers() {
		List<int> result = new List<int> ();
		Node node = firstNode;

		if (node != null) {
			result.Add(firstNode.getPlanet().getNumber());
		}
		while (node.getNext () != Vector3.zero) {
			node= galaxyMap[node.getNext ()];
			result.Add (node.getPlanet ().getNumber());
		}
	
   		return result;
	}

	public void enableInput() {
		foreach (Button child in GameManagerScript.nodePanel.GetComponentsInChildren<Button>()) {
			child.interactable= true;
		}	
		foreach (InputField child in GameManagerScript.nodePanel.GetComponentsInChildren<InputField>()) {
			child.interactable= true;
		}
	}
	
	public void disableInput() {
		foreach (Button child in GameManagerScript.nodePanel.GetComponentsInChildren<Button>()) {
			child.interactable= false;
		}	
		foreach (InputField child in GameManagerScript.nodePanel.GetComponentsInChildren<InputField>()) {
			child.interactable= false;
		}
	}
}
